// Copyright (C) 2010-2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// In this script, we search failure cases for all algorithms.
// I notice that Scilab/x87 produce correct results in the 
// experiments we checked. 
// I think that this is caused by the extended precision of the x87 registers
// which are used by Scilab on this Linux 32 bits system.
// Hence, I put Scilab/x87 (i.e. cdiv_scilab) as the reference.
// I try divisions defined by [a,b,c,d]=[2^na,2^nb,2^nc,2^nd],
// where na,nb,nc,nd are in [-1074 1023].
// I perform kmax such divisions, with kmax a large integer, 
// kmax=100 000 for example.
// I count the number of times k a particular algorithm, say Smith for example,
// produced a different result (and non-nan, non-inf).
// I assume that Scilab/x87 is always correct, so that k 
// is a count of the failures.
// The probability of failure is then p=k/kmax.
// I think that this may be a reasonable measure of failure probability,
// because failures in the division are most of the times caused 
// by an underflow/overflow in a multiplication/division.
// Hence, there should be a very small number of failure caused 
// by overflow in an addition.
//
// The variance is V=p*(1-p)/N, t
// The 95% interval confidence is [p-1.96*s,p+1.96*s],
// where s=sqrt(V).
//
// The results:
nmatrix = [
-1074:1023
-1074:1023
-1074:1023
-1074:1023
];
// Naive   vs Scilab/x87 : p=1.222e-1, s=5.054e-4, 95% interval = [1.21e-1,1.23e-1]
// Smith   vs Scilab/x87 : p=1.298e-2, s=1.132e-4, 95% interval = [1.27e-2,1.32e-2]
// SmithLi vs Scilab/x87 : p=1.296e-2, s=1.460e-4, 95% interval = [1.27e-2,1.32e-2]
// Smith4  vs Scilab/x87 : p=2.310e-4, s=1.520e-5, 95% interval = [2.01e-4,2.61e-4]
// Smith5  vs Scilab/x87 : p=2.560e-4, s=1.600e-5, 95% interval = [2.25e-4,2.87e-4]
// Smith6  vs Scilab/x87 : p=2.920e-4
// Smith7  vs Scilab/x87 : p=7.900e-5, s=8.888e-6, 95% interval = [6.16e-5,9.64e-5]
// Smith8  vs Scilab/x87 : p=1.000e-4
// Smith9  vs Scilab/x87 : p=9.311e-1
// Priest  vs Scilab/x87 : p=3.955e-2, s=1.949e-4, 95% interval =[3.92e-2,3.99e-2]
// Stewart vs Scilab/x87 : p=2.556e-4, s=1.884e-5, 95% interval =[2.19e-4,2.92e-4]
// C99     vs Scilab/x87 : p=1.349e-2, s=3.845e-4, 95% interval =[1.27e-2,1.42e-2]



// Some attempts to improve Smith's algorithm


ieee(2);


////////////////////////////////////////////////////
//
nmatrix = [
-1074:1023
-1074:1023
-1074:1023
-1074:1023
];
kmax = 1000000;
stacksize("max");

mprintf("Naive\n");
verbose = 1;
compdiv_searchfordiff ( compdiv_naive , compdiv_scilab , nmatrix , kmax , 1.e-2 , 0 , verbose );

mprintf("Smith\n");
compdiv_searchfordiff ( compdiv_smith , compdiv_scilab , nmatrix , kmax , 5.e-2 , 0 , verbose );

mprintf("Li\n");
compdiv_searchfordiff ( compdiv_smithLi , compdiv_scilab , nmatrix , kmax , 5.e-2 , 0 , verbose );

mprintf("Improved\n");
compdiv_searchfordiff ( compdiv_smith4 , compdiv_scilab , nmatrix , kmax , 5.e-2 , 0 , verbose );

mprintf("Priest\n");
compdiv_searchfordiff ( compdiv_priest , compdiv_scilab , nmatrix , kmax , 5.e-2 , 0 , verbose )

mprintf("Stewart\n");
compdiv_searchfordiff ( compdiv_smithStewart , compdiv_scilab , nmatrix , kmax , 5.e-2 , 0 , verbose )

mprintf("C99\n");
compdiv_searchfordiff ( compdiv_ansiisoc , compdiv_scilab , nmatrix , kmax , 5.e-2 , 0 , verbose )


////////////////////////////////////////////////////
//
// Consider exponents well within the achievable bounds.
//
nmatrix = [
-900:900
-900:900
-900:900
-900:900
];
kmax = 1000000;
stacksize("max");

mprintf("Naive\n");
compdiv_searchfordiff ( compdiv_naive , compdiv_scilab , nmatrix , kmax , 1.e-2 , 0 , verbose );

mprintf("Smith\n");
compdiv_searchfordiff ( compdiv_smith , compdiv_scilab , nmatrix , kmax , 5.e-2 , 0 , verbose );

mprintf("Li\n");
compdiv_searchfordiff ( compdiv_smithLi , compdiv_scilab , nmatrix , kmax , 5.e-2 , 0 , verbose );

mprintf("Improved\n");
compdiv_searchfordiff ( compdiv_smith4 , compdiv_scilab , nmatrix , kmax , 5.e-2 , 0 , verbose );

mprintf("Priest\n");
compdiv_searchfordiff ( compdiv_priest , compdiv_scilab , nmatrix , kmax , 5.e-2 , 0 , verbose )

mprintf("Stewart\n");
compdiv_searchfordiff ( compdiv_smithStewart , compdiv_scilab , nmatrix , kmax , 5.e-2 , 0 , verbose )

mprintf("C99\n");
compdiv_searchfordiff ( compdiv_ansiisoc , compdiv_scilab , nmatrix , kmax , 5.e-2 , 0 , verbose )

if ( %f ) then
mprintf("S5\n");
compdiv_searchfordiff ( compdiv_smith5 , compdiv_scilab , nmatrix , kmax , 1.e-2 , 0 , verbose );

mprintf("S7\n");
compdiv_searchfordiff ( compdiv_smith7 , compdiv_scilab , nmatrix , kmax , 1.e-2 , 0 , verbose )

mprintf("S9\n");
compdiv_searchfordiff ( compdiv_smith9 , compdiv_scilab , nmatrix , kmax , 1.e-2 , 0 , verbose )

mprintf("S8\n");
compdiv_searchfordiff ( compdiv_smith8 , compdiv_scilab , nmatrix , kmax , 1.e-2 , 0 , verbose )
end

////////////////
// Search for worst cases

if ( %f ) then
compdiv_searchfordiffWorst ( compdiv_smith7 , compdiv_scilab , 40 );
compdiv_searchfordiffWorst ( compdiv_smith5 , compdiv_scilab , 40 );
end

////////////////////////////////////////////////////
//
// Consider difficult cases
nmatrix = [-1074:-1010 1010:1023; -1074:-1010 1010:1023; -1074:-1010 1010:1023; -1074:-1010 1010:1023];

rtol = 1.e-1;
atol = 0;
verbose = 1;
verboserate = 1000;

mprintf("Naive\n");
compdiv_searchfordiff ( compdiv_naive , compdiv_scilab , nmatrix , kmax , rtol , atol , verbose , verboserate );

mprintf("Smith\n");
compdiv_searchfordiff ( compdiv_smith , compdiv_scilab , nmatrix , kmax , rtol , atol , verbose , verboserate );

mprintf("Li\n");
compdiv_searchfordiff ( compdiv_smithLi , compdiv_scilab , nmatrix , kmax , rtol , atol , verbose , verboserate );

mprintf("Improved\n");
compdiv_searchfordiff ( compdiv_smith4 , compdiv_scilab , nmatrix , kmax , rtol , atol , verbose , verboserate );

mprintf("Priest\n");
compdiv_searchfordiff ( compdiv_priest , compdiv_scilab , nmatrix , kmax , rtol , atol , verbose , verboserate );

mprintf("Stewart\n");
compdiv_searchfordiff ( compdiv_smithStewart , compdiv_scilab , nmatrix , kmax , rtol , atol , verbose , verboserate );

mprintf("C99\n");
compdiv_searchfordiff ( compdiv_ansiisoc , compdiv_scilab , nmatrix , kmax , rtol , atol , verbose , verboserate );

