// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt



function t=benchcompdivT ( N , divfun )
  a = grand(N,1,"def");
  b = grand(N,1,"def");
  c = grand(N,1,"def");
  d = grand(N,1,"def");
  x=complex(a,b);
  y=complex(c,d);
  tic ()
  z=divfun(x,y)
  t = toc()
  //mprintf("N=%d, t=%f (s)\n",N,t)
endfunction

//
timemax=0.2;
mprintf("Benchmarking compdiv_scilab...\n");
perftable1 = scibench_dynbenchfun ( list(benchcompdivT,compdiv_scilab) , [], [],[],timemax);
mprintf("Benchmarking compdiv_fimproved...\n");
perftable2 = scibench_dynbenchfun ( list(benchcompdivT,compdiv_fimproved) , [], [],[],timemax);
mprintf("Benchmarking compdiv_frobust...\n");
perftable3 = scibench_dynbenchfun ( list(benchcompdivT,compdiv_frobust) , [], [],[],timemax);
mprintf("Benchmarking compdiv_priest...\n");
perftable4 = scibench_dynbenchfun ( list(benchcompdivT,compdiv_priest) , [], [],[],timemax);
mprintf("Benchmarking compdiv_fstewart...\n");
perftable5 = scibench_dynbenchfun ( list(benchcompdivT,compdiv_fstewart) , [], [],[],timemax);
scf();
plot(perftable1(:,1),perftable1(:,2),"bo");
plot(perftable2(:,1),perftable2(:,2),"rx");
plot(perftable3(:,1),perftable3(:,2),"g*");
plot(perftable4(:,1),perftable4(:,2),"ks");
plot(perftable5(:,1),perftable5(:,2),"rs");
xtitle("Performance of complex divisions","Data size (n)","Time (s)");
legend(["compdiv_scilab","compdiv_fimproved","compdiv_frobust","compdiv_priest","compdiv_fstewart"]);

if ( %f ) then
//
// Pre-registered datas
perftable1 = [33,1595,1914,8237,9885,11862,17082,20499,24599,29519,35423,42508,51010,61212,73455,88146,..  
 105776,126932,152319,182783,219340,263208,315850,379020,454824,545789,654947,785937,..       
 943125,1131750;                                                                              
0.008,0.004,0.004,0.002,0.002,0.002,0.006,0.006,0.012,0.008,0.015,0.016,0.014,0.028,..        
 0.021,0.036,0.027,0.023,0.028,0.034,0.042,0.048,0.06,0.073,0.088,0.102,0.123,0.148,0.185,..  
 0.217]';
perftable2=[27,101,308,444,1914,6864,9885,11862,14235,17082,20499,24599,29519,35423,42508,51010,..      
 61212,73455,88146,105776,126932,152319,182783,219340,263208,315850,379020,454824,545789,..  
 654947,785937,943125,1131750;                                                               
0.002,0.002,0.01,0.008,0.008,0.002,0.007,0.002,0.003,0.015,0.003,0.013,0.034,0.01,0.016,..   
 0.019,0.032,0.025,0.03,0.02,0.022,0.027,0.032,0.04,0.047,0.055,0.066,0.081,0.097,0.116,..   
 0.141,0.17,0.204]';
perftable3=[5720,6864,8237,9885,11862,14235,17082,20499,24599,29519,35423,42508,51010,61212,73455,..    
 88146,105776,126932,152319,182783,219340,263208,315850,379020,454824,545789,654947,..       
 785937;                                                                                     
0.002,0.007,0.007,0.012,0.003,0.008,0.018,0.034,0.015,0.025,0.022,0.02,0.046,0.045,0.045,..  
 0.047,0.038,0.035,0.043,0.051,0.062,0.075,0.089,0.108,0.132,0.157,0.185,0.227]';

perftable4=[768,3971,8237,9885,11862,14235,17082,20499,24599,29519,35423,42508,51010,61212,73455,..  
 88146,105776,126932,152319,182783,219340,263208,315850,379020,454824,545789,654947,..    
 785937,943125,1131750;                                                                   
0.002,0.005,0.002,0.002,0.008,0.003,0.011,0.012,0.014,0.014,0.027,0.016,0.027,0.031,..    
 0.032,0.035,0.031,0.025,0.032,0.038,0.044,0.054,0.065,0.078,0.093,0.111,0.134,0.162,..   
 0.195,0.259]';
scf();
plot(perftable1(:,1),perftable1(:,2),"bo");
plot(perftable2(:,1),perftable2(:,2),"rx");
plot(perftable3(:,1),perftable3(:,2),"g*");
plot(perftable4(:,1),perftable4(:,2),"ks");
xtitle("Performance of complex divisions","Data size (n)","Time (s)");
legend(["compdiv_scilab","compdiv_fimproved","compdiv_frobust","compdiv_priest"]);
end
 

