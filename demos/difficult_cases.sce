// Copyright (C) 2010-2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


mprintf("Scilab:\n");
compdiv_difficultcases(compdiv_scilab);
mprintf("Smith:\n");
compdiv_difficultcases(compdiv_smith);
mprintf("Naive:\n");
compdiv_difficultcases(compdiv_naive);
mprintf("Stewart:\n");
compdiv_difficultcases(compdiv_stewart);
mprintf("Li:\n");
compdiv_difficultcases(compdiv_smithLi);
mprintf("Priest:\n");
compdiv_difficultcases(compdiv_priest);
mprintf("C99:\n");
compdiv_difficultcases(compdiv_ansiisoc);
mprintf("Improved:\n");
compdiv_difficultcases(compdiv_improved);
mprintf("Robust:\n");
compdiv_difficultcases(compdiv_robust);
mprintf("Fast Improved:\n");
compdiv_difficultcases(compdiv_fimproved);
mprintf("Fast Robust:\n");
compdiv_difficultcases(compdiv_frobust);

