// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Measure the accuracy on all the difficult cases, 
// on all the complex division algorithms.
divlist = [
  "compdiv_naive"
  "compdiv_smith"
  "compdiv_stewart"
  "compdiv_smithLi"
  "compdiv_ansiisoc"
  "compdiv_priest"
  "compdiv_improved"
  "compdiv_robust"
  ];
nbdiv = size(divlist,"*");
// results(k,i) is the number of accurate bits for algorithm #i on division #k.
results = [];
[x,y,z] = compdiv_difficultcases();
n = size(x,"*");
ieee(2);
for i = 1 : nbdiv
  divstr = divlist(i);
  funpr = funcprot();
  funcprot(0);
  execstr("divfun ="+ divstr);
  funcprot(funpr);
  [x,y,z,q,digits] = compdiv_difficultcases(divfun,%f);
  results(1 : n,i) = floor(digits(1 : n));
end
// Put the total in row n+1
results(n+1,:) = sum(results(1:n,:),"r");
disp(results);


// Format the result for humans...
resultsh = [];
resultsh(1,1) = "Case";
for i = 1 : nbdiv
  resultsh(1,i+1) = divlist(i);
end
for k = 1 : n
  resultsh(k+1,1) = string(k);
end
resultsh(n+2,1) = "Total";
for i = 1 : nbdiv
  for k = 1 : n+1
    resultsh(k+1,i+1) = string(results(k,i));
  end
end
disp(resultsh);

