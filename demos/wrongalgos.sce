// Copyright (C) 2010-2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// A collection of poor algorithms

function z = compdiv_smith2 ( x,y )
    // An improved Smith method, with ideas from Stewart.
    //
    // Calling Sequence
    // z = compdiv_smith2 ( x,y )
    //
    // Parameters
    // x : a matrix of complex doubles
    // y : a matrix of complex doubles
    // z : a matrix of complex doubles, the complex division result z=x/y.
    //
    // Description
    // Computes the complex division z=x/y, using a 
    // product function based on a recursive product 
    // of the minimum times the maximum magnitude number.
    //
    // Examples
    // expected = 11/25 + %i * 2/25
    // compdiv_smith2 ( 1+2*%i , 3+4*%i )
    //
    // Authors
    // Copyright (C) 2010 - Michael Baudin

    a = real(x)
    b = imag(x)
    c = real(y)
    d = imag(y)
    if ( abs(d) <= abs(c) ) then
        z = 1 / c
        den = c + d * (d * z)
        e = (a + prodminmax ( [ b d z ] ) ) / den
        f = (b - prodminmax ( [ a d z ] ) ) / den
    else
        z = 1 / d
        den = c * (c * z) + d
        e = ( prodminmax ( [ a c z ] ) + b) / den
        f = ( prodminmax ( [ b c z ] ) - a) / den
    end
    // Avoid using x + %i * y, which may create
    // %inf+%i*%inf, which actually multiplies 0 and %inf,
    // generating an unwanted %nan.
    z = complex(e,f)
endfunction
// A stable floating point product of a vector.
// Based on Stewart's ideas
function p = prodminmax ( v )
    n = size(v,"*")
    v = matrix(v,n,1)
    for i = 1 : n - 1
        // Search the maximum absolute value
        [ma,kmax]=max(abs(v))
        // Search the minimum absolute value in other values
        o = [v(1:kmax-1);v(kmax+1:$)]
        [mi,komin]=min(abs(o))
        // komin is the index in o
        // Compute the index kmin in v.
        if ( komin<=kmax-1 ) then
            kmin = komin
        else
            kmin = komin+1
        end
        v(kmin) = v(kmax) * v(kmin)
        v(kmax) = []
    end
    p = v(1)
endfunction

// A stable floating point product of a vector.
// Based on Stewart's ideas
// Does not delete entries in the vector v.
// More clever, but longer: forces to generate the matrices (1:i).
function p = prodminmax2 ( v )
    n = size(v,"*")
    v = matrix(v,n,1)
    for i = n-1 : -1 : 1
        // Search the maximum absolute value in v(1:i+1)
        [w,k]=max(abs(v(1:i+1)))
        // Switch the max and v(i+1)
        v([i+1,k]) = v([k,i+1])
        // Search the minimum absolute value in v(1:i)
        [w,k]=min(abs(v(1:i)))
        // Put the product into the min
        v(k) = v(k) * v(i+1)
    end
    p = v(1)
endfunction

if ( %f ) then
    p = prodminmax ( [1 3 1 2] ) // 6
    p = prodminmax ( [-1 2 -3 4 1 -2 3 4] ) //  -576
    p = prodminmax ( [-1 1 1] )   // -1
    p = prodminmax ( [-1 -1 1] )  //  1
    p = prodminmax ( [-1 -1 -1] ) // -1
    p = prodminmax ( [-1 2 3] )    // -6
    p = prodminmax ( [-1 -2 3] )   //  6
    p = prodminmax ( [-1 2 -3] )   //  6
    p = prodminmax ( [1 2 -3] )   //  -6
    p = prodminmax ( [1.e200 1.e200 1.e-100] ) //  1.+300
    for k = 1 : 10
        v=round(10*rand(10,1)-5);
        v(find(v==0))=[];
        p1 = prodminmax ( v );
        p2 = prod ( v );
        disp([k p1 p2])
    end
    //
    p = prodminmax2 ( [1 3 1 2] ) // 6
    p = prodminmax2 ( [-1 2 -3 4 1 -2 3 4] ) //  -576
    p = prodminmax2 ( [-1 1 1] )   // -1
    p = prodminmax2 ( [-1 -1 1] )  //  1
    p = prodminmax2 ( [-1 -1 -1] ) // -1
    p = prodminmax2 ( [-1 2 3] )    // -6
    p = prodminmax2 ( [-1 -2 3] )   //  6
    p = prodminmax2 ( [-1 2 -3] )   //  6
    p = prodminmax2 ( [1 2 -3] )   //  -6
    p = prodminmax2 ( [1.e200 1.e200 1.e-100] ) //  1.+300
    for k = 1 : 10
        v=round(10*rand(10,1)-5);
        v(find(v==0))=[];
        p1 = prodminmax2 ( v );
        p2 = prod ( v );
        disp([k p1 p2])
    end
end
if ( %f ) then
    //
    // Test perfs
    exec("benchfun.sci")
    n=5000;
    v=round(10*rand(n,1)-5);
    benchfun ( "prodminmax " , prodminmax , list(v) , 1 , 10 );
    benchfun ( "prodminmax2" , prodminmax2 , list(v) , 1 , 10 );
    benchfun ( "prod       " , prod , list(v) , 1 , 10 );
end

function z = compdiv_smith3 (x,y)
    // Performs complex division with an improved Smith method (exp-log product).
    //
    // Calling Sequence
    // z = compdiv_smith3 ( x,y )
    //
    // Parameters
    // x : a matrix of complex doubles
    // y : a matrix of complex doubles
    // z : a matrix of complex doubles, the complex division result z=x/y.
    //
    // Description
    // Computes the complex division z=x/y, using a 
    // product function based on a combination of exp and log.
    // Can be very inaccurate.
    //
    // Examples
    // expected = 11/25 + %i * 2/25
    // compdiv_smith3 ( 1+2*%i , 3+4*%i )
    //
    // Authors
    // Copyright (C) 2010 - Michael Baudin

    a = real(x)
    b = imag(x)
    c = real(y)
    d = imag(y)
    if ( abs(d) <= abs(c) ) then
        z = 1 / c
        den = c + d * (d * z)
        e = (a + prodexplog ( [ b d z ] ) ) / den
        f = (b - prodexplog ( [ a d z ] ) ) / den
    else
        z = 1 / d
        den = c * (c * z) + d
        e = ( prodexplog ( [ a c z ] ) + b) / den
        f = ( prodexplog ( [ b c z ] ) - a) / den
    end
    // Avoid using x + %i * y, which may create
    // %inf+%i*%inf, which actually multiplies 0 and %inf,
    // generating an unwanted %nan.
    z = complex(e,f)
endfunction

// A product of a vector.
// Based on log(exp) trick : cost much more !
function p = prodexplog ( v )
    pe = exp(sum(log(v)))
    p = abs(pe) * sign(real(pe))
endfunction

if ( %f ) then
    p = prodexplog ( [1 3 1 2] ) // 6
    p = prodexplog ( [-1 2 -3 4 1 -2 3 4] ) //  -576
    p = prodexplog ( [-1 1 1] )   // -1
    p = prodexplog ( [1 -1 1] )   // -1
    p = prodexplog ( [1.e200 1.e200 1.e-100] ) //  1.+300

end

function z = compdiv_smith5 ( x, y )
    // Improved Complex Division - Robert L. Smith - Michael Baudin - 2010
    //
    // Calling Sequence
    // z = compdiv_smith5 ( x,y )
    //
    // Parameters
    // x : a matrix of complex doubles
    // y : a matrix of complex doubles
    // z : a matrix of complex doubles, the complex division result z=x/y.
    // 
    // Description
    // This implementation uses the improved complex division algorithm (as in compdiv_improved) and 
    // and extra down scaling of a, b, c and d.
    // The down scaling is the same as Li et al.
    //
    // Examples
    // expected = 11/25 + %i * 2/25
    // compdiv_smith5 ( 1+2*%i , 3+4*%i )
    //
    // Authors
    // Copyright (C) 2010 - Robert L. Smith - Michael Baudin

    a = real(x)
    b = imag(x)
    c = real(y)
    d = imag(y)
    AB = max(abs([a b]))
    CD = max(abs([c d]))
    S = 1
    OV = number_properties("huge")
    // Scaling for overflow
    SU = 16
    if ( AB > OV/SU ) then
        // Scale down a, b
        a = a/SU
        b = b/SU
        S = S*SU
    end
    if ( CD > OV/SU ) then
        // Scale down c, d
        c = c/SU
        d = d/SU
        S = S/SU
    end
    // Apply improved Smith.
    z = compdiv_improved ( complex(a,b), complex(c,d) )
    e = real(z)
    f = imag(z)
    // Scale back
    e = e * S
    f = f * S
    // Avoid using x + %i * y, which may create
    // %inf+%i*%inf, which actually multiplies 0 and %inf,
    // generating an unwanted %nan.
    z = complex(e,f)
endfunction


function z = compdiv_smith6 ( x, y )
    // Improved Complex Division - Robert L. Smith - Michael Baudin - 2010
    //
    // Calling Sequence
    // z = compdiv_smith6 ( x,y )
    //
    // Parameters
    // x : a matrix of complex doubles
    // y : a matrix of complex doubles
    // z : a matrix of complex doubles, the complex division result z=x/y.
    // 
    // Description
    // This implementation uses the improved complex division algorithm (as in compdiv_improved) and 
    // expanded expressions.
    // For example, we use the expression e = a * t + b * r * t instead 
    // of e = (a + b * r) * t.
    // This prevents overflows in some situations.
    //
    // Examples
    // expected = 11/25 + %i * 2/25
    // compdiv_smith6 ( 1+2*%i , 3+4*%i )
    //
    // Authors
    // Copyright (C) 2010 - Robert L. Smith - Michael Baudin

    a = real(x)
    b = imag(x)
    c = real(y)
    d = imag(y)
    if ( abs(d) <= abs(c) ) then
        r = d/c
        t = 1/(c + d * r)
        if (r <> 0) then
            e = a * t + b * r * t
            f = b * t - a * r * t
        else
            e = a * t + d * (b/c) * t
            f = b * t - d * (a/c) * t
        end
        // Treat the cases which produce Nans
        if ( isnan(e) ) then
            e = (a + b * r ) * t
        end
        if ( isnan(f) ) then
            f = (b - a * r ) * t
        end
    else
        r = c/d
        t = 1/(c * r + d  )
        if (r <> 0) then
            e = a * r * t + b * t
            f = b * r * t - a * t
        else
            e = c * (a/d) * t + b * t
            f = c * (b/d) * t - a * t
        end
        // Treat the cases which produce Nans
        if ( isnan(e) ) then
            e = (a*r + b ) * t
        end
        if ( isnan(f) ) then
            f = (b*r - a ) * t
        end
    end
    // Avoid using x + %i * y, which may create
    // %inf+%i*%inf, which actually multiplies 0 and %inf,
    // generating an unwanted %nan.
    z = complex(e,f)
endfunction


function z = compdiv_smith7 ( x, y )
    // Improved Complex Division - Robert L. Smith - Michael Baudin - 2010
    //
    // Calling Sequence
    // z = compdiv_smith7 ( x,y )
    //
    // Parameters
    // x : a matrix of complex doubles
    // y : a matrix of complex doubles
    // z : a matrix of complex doubles, the complex division result z=x/y.
    // 
    // Description
    // An improved complex division, based on a improved 
    // Smith algorithm (as in compdiv_improved).
    // Additionnally, we expand the formulas, but only when necessary.
    // This algorithms almost never fails. 
    // When it fails, it loses, most of the time, at most 1 binary digits, i.e. it 
    // accuracy is most of the time greater than 52 digits.
    // 
    // TODO : expand all the expressions, including the case r=0.
    // TODO : test if br is zero or inf or nan (create a issuspect function).
    //
    // Examples
    // expected = 11/25 + %i * 2/25
    // compdiv_smith7 ( 1+2*%i , 3+4*%i )
    //
    // // A known case of failure is
    // x = 2^-347+%i*2^-54;
    // y = 2^-1037+%i*2^-1058;
    // z = 3.898125604559113300e289 + %i * 8.174961907852353577e295;
    // compdiv_smith7 ( x, y )
    //
    // Authors
    // Copyright (C) 2010 - Robert L. Smith - Michael Baudin

    a = real(x)
    b = imag(x)
    c = real(y)
    d = imag(y)
    if ( abs(d) <= abs(c) ) then
        r = d/c
        t = 1/(c + d * r)
        if (r <> 0) then
            // e = (a + b * r) * t
            br = b*r
            if ( br <> 0 ) then
                e = (a + b * r) * t
            else
                e = a*t + b * (r*t)
            end
            ar = a * r
            if ( ar <> 0 )
                f = (b - a * r) * t
            else
                f = b*t - a * (r*t)
            end
        else
            e = (a + d * (b/c)) * t
            f = (b - d * (a/c)) * t
        end
    else
        r = c/d
        t = 1/(c * r + d  )
        if (r <> 0) then
            // e = (ar + b) * t
            ar = a * r
            if ( ar <> 0 ) then
                e = (ar + b) * t
            else
                e = a * (r*t) + b * t
            end
            br = b*r
            if ( br <> 0 ) then
                f = (br - a) * t
            else
                f = b * (r*t) - a*t
            end
        else
            e = (c * (a/d) + b) * t
            f = (c * (b/d) - a) * t
        end
    end
    // Avoid using x + %i * y, which may create
    // %inf+%i*%inf, which actually multiplies 0 and %inf,
    // generating an unwanted %nan.
    z = complex(e,f)
endfunction

function z = compdiv_smith8 ( x,y )
    //   Returns the complex division z = x / y  by an improved Smith's method.
    //
    // Calling Sequence
    // z = compdiv_smith8 ( x,y )
    //
    // Parameters
    // x : a matrix of complex doubles
    // y : a matrix of complex doubles
    // z : a matrix of complex doubles, the complex division result z=x/y.
    //
    // Description
    // Uses an improved Smith's algorithm to perform the complex division, as in 
    // compdiv_improved.
    // We additionnally use a robust floating point multiplication (compdiv_prodfrexp), based on the 
    // frexp function.
    //
    // Examples
    // expected = 11/25 + %i * 2/25
    // compdiv_smith8 ( 1+2*%i , 3+4*%i )
    //
    // x = rand(3,5)+%i*rand(3,5)
    // y = rand(3,5)+%i*rand(3,5)
    // z = compdiv_naive ( x , y )
    // abs(z.*y - x)./abs(x)
    //
    // Authors
    // Copyright (C) 2010 - Robert L. Smith - Michael Baudin
    
    for i = 1 : size(x,"r")
    for j = 1 : size(x,"c")
    z(i,j) = compdiv_smith8S ( x(i,j) , y(i,j) )
    end
    end
endfunction

function z = compdiv_smith8S ( x , y )
    a = real(x)
    b = imag(x)
    c = real(y)
    d = imag(y)
    if ( abs(d) <= abs(c) ) then
        r = d/c
        t = 1/(c + d * r)
        if (r <> 0) then
            e = a * t + compdiv_prodfrexp ( [b r t] )
            f = b * t - compdiv_prodfrexp ( [a r t] )
        else
            e = a * t + compdiv_prodfrexp ( [d (b/c) t] ) 
            f = b * t - compdiv_prodfrexp ( [d (a/c) t] ) 
        end
        // Treat the cases which produce Nans
        if ( isnan(e) ) then
            e = (a + b * r ) * t
        end
        if ( isnan(f) ) then
            f = (b - a * r ) * t
        end
    else
        r = c/d
        t = 1/(c * r + d  )
        if (r <> 0) then
            e = compdiv_prodfrexp ( [a r t] ) + b * t
            f = compdiv_prodfrexp ( [b r t] ) - a * t
        else
            e = compdiv_prodfrexp ( [c (a/d) t] ) + b * t
            f = compdiv_prodfrexp ( [c (b/d) t] ) - a * t
        end
        // Treat the cases which produce Nans
        if ( isnan(e) ) then
            e = (a*r + b ) * t
        end
        if ( isnan(f) ) then
            f = (b*r - a ) * t
        end
    end
    // Avoid using x + %i * y, which may create
    // %inf+%i*%inf, which actually multiplies 0 and %inf,
    // generating an unwanted %nan.
    z = complex(e,f)
endfunction

function r = compdiv_smith10 ( x, y )
    // Improved Complex Division - Robert L. Smith - Michael Baudin - 2010
    //
    // Calling Sequence
    // r = compdiv_smith10 ( x,y )
    //
    // Parameters
    // x : a matrix of complex doubles
    // y : a matrix of complex doubles
    // r : a matrix of complex doubles, the complex division result r=x/y.
    // 
    // Description
    // An improved complex division, based on a improved 
    // Smith algorithm as in compdiv_improved.
    // Try to prevent overflow in t=1/(c+d*r) by scaling by 
    // a power of two, as in Li et al..
    // Try to prevent underflow when possible by scaling by a or c or d.
    // Try to prevent overflow by sum in e or f, by expanding
    // the expression.
    //
    // This algorithms almost never fails, i.e. produces 
    // more than 52 significant digits (instead of the maximum 
    // 53) most of the times. 
    // It can perform more than 1 000 000 random divisions with
    // less than a binary digit wrong.
    //
    // TODO : simplify it as much as possible.
    //
    // Examples
    // expected = 11/25 + %i * 2/25
    // compdiv_smith10 ( 1+2*%i , 3+4*%i )
    //
    // // Known difficult case (solved)
    // x = 2^-1074+%i*2^-1074;
    // y = 2^-1073+%i*2^-1074;
    // z=0.6+%i*0.2;
    // compdiv_smith10 ( x , y )
    //
    // // A difficult case (unsolved)
    // x = 2^1015+%i*2^-989;
    // y = 2^1023+%i*2^1023;
    // z = 0.0019531+%i*-0.0019531;
    // r = compdiv_smith10 ( x , y )
    //
    // Authors
    // Copyright (C) 2010 - Robert L. Smith - Michael Baudin
    
    a = real(x)
    b = imag(x)
    c = real(y)
    d = imag(y)
    if ( abs(d) <= abs(c) ) then
        r = d/c
        t = 1/(c + d * r)
        if ( abs(t) == %inf ) then
            S = 2/%eps^2
            // Scale up c, d
            c = c * S
            d = d * S
            // Recursive call
            z = compdiv_smith10 ( x, complex(c,d) )
            // Scale back
            e = real(z) * S
            f = imag(z) * S
        elseif (r <> 0) then
            // e = (a + b * r) * t
            br = b*r
            if ( br <> 0 ) then
                e = (a + b * r) * t
                if ( abs(e) == %inf | isnan(e) | e==0 ) then 
                    if ( abs(a)>abs(c) ) then
                        e = (1+(b/a)*r)/((c/a)+(d/a)*r)
                    else
                        e = ((a/c)+(b/c)*r)/(1+(d/c)*r)
                    end
                end
            else
                e = a*t + b * (r*t)
            end
            ar = a * r
            if ( ar <> 0 )
                f = (b - a * r) * t
            else
                f = b*t - a * (r*t)
            end
        else
            e = (a + d * (b/c)) * t
            f = (b - d * (a/c)) * t
        end
    else
        r = c/d
        t = 1/(c * r + d)
        if ( abs(t) == %inf ) then
            S = 2/%eps^2
            // Scale up c, d
            c = c * S
            d = d * S
            // Recursive call
            z = compdiv_smith10 ( x, complex(c,d) )
            // Scale back
            e = real(z) * S
            f = imag(z) * S
        elseif (r <> 0) then
            // e = (ar + b) * t
            ar = a * r
            if ( ar <> 0 ) then
                e = (ar + b) * t
                if ( abs(e) == %inf | isnan(e) | e==0 ) then
                    if ( abs(b)>abs(d) ) then
                        e = ((a/b)*r+1)/((c/b)*r+d/b)
                    else
                        e = ((a/d)*r+b/d)/((c/d)*r+1)
                    end
                end
            else
                e = a * (r*t) + b * t
            end
            br = b*r
            if ( br <> 0 ) then
                f = (br - a) * t
            else
                f = b * (r*t) - a*t
            end
        else
            e = (c * (a/d) + b) * t
            f = (c * (b/d) - a) * t
        end
    end
    // Avoid using x + %i * y, which may create
    // %inf+%i*%inf, which actually multiplies 0 and %inf,
    // generating an unwanted %nan.
    r = complex(e,f)
endfunction

function z = compdiv_smith11 ( x, y )
    // Improved Complex Division - Robert L. Smith - Michael Baudin - 2010
    //
    // Calling Sequence
    // z = compdiv_smith11 ( x,y )
    //
    // Parameters
    // x : a matrix of complex doubles
    // y : a matrix of complex doubles
    // z : a matrix of complex doubles, the complex division result z=x/y.
    // 
    // Description
    // An improved complex division, based on a improved 
    // Smith algorithm, with an additionnal case.
    // Additionnally, try to prevent underflow or overflow when possible, 
    // by trying all expressions in compdiv_evaluate.
    //
    // This algorithms almost never fails, i.e. produces 
    // more than 52 significant digits (instead of the maximum 
    // 53) most of the times. 
    // It can perform more than 1 000 000 random divisions with
    // less than a binary digit wrong.
    //
    // TODO : TEST THIS
    //
    // Examples
    // expected = 11/25 + %i * 2/25
    // compdiv_smith11 ( 1+2*%i , 3+4*%i )
    //
    // // Known difficult case (solved)
    // x = 2^-1074+%i*2^-1074;
    // y = 2^-1073+%i*2^-1074;
    // z=0.6+%i*0.2;
    // compdiv_smith11 ( x , y )
    //
    // // A difficult case (unsolved)
    // x = 2^1015+%i*2^-989;
    // y = 2^1023+%i*2^1023;
    // z = 0.0019531+%i*-0.0019531;
    // z = compdiv_smith11 ( x , y )
    //
    // Authors
    // Copyright (C) 2010-2011 - Robert L. Smith - Michael Baudin

    a = real(x)
    b = imag(x)
    c = real(y)
    d = imag(y)
    if ( abs(d) <= abs(c) ) then
        r = d/c
        t = 1/(c + d * r)
        if (r <> 0) then
            // e = (a + b * r) * t
            [e,m] = compdiv_tryevaluate ( x , y , 1 )
            // f = (b - a * r) * t
            [f,m] = compdiv_tryevaluate ( x , y , 2 )
        else
            // e = (a + d * (b/c)) * t
            [e,m] = compdiv_tryevaluate ( x , y , 3 )
            // f = (b - d * (a/c)) * t
            [f,m] = compdiv_tryevaluate ( x , y , 4 )
        end
        // Treat the cases which produce Nans
        if ( isnan(e) ) then
            e = (a + b * r ) * t
        end
        if ( isnan(f) ) then
            f = (b - a * r ) * t
        end
    else
        r = c/d
        t = 1/(c * r + d)
        if (r <> 0) then
            // e = (ar + b) * t
            [e,m] = compdiv_tryevaluate ( x , y , 5 )
            // f = (br - a) * t
            [f,m] = compdiv_tryevaluate ( x , y , 6 )
        else
            // e = (c * (a/d) + b) * t
            [e,m] = compdiv_tryevaluate ( x , y , 7 )
            // f = (c * (b/d) - a) * t
            [f,m] = compdiv_tryevaluate ( x , y , 8 )
        end
        // Treat the cases which produce Nans
        if ( isnan(e) ) then
            e = (a*r + b ) * t
        end
        if ( isnan(f) ) then
            f = (b*r - a ) * t
        end
    end
    // Avoid using x + %i * y, which may create
    // %inf+%i*%inf, which actually multiplies 0 and %inf,
    // generating an unwanted %nan.
    z = complex(e,f)
endfunction

function z = compdiv_smith13 ( x, y )
    // Improved Complex Division - Robert L. Smith - Michael Baudin - 2010
    //
    // Calling Sequence
    // z = compdiv_smith13 ( x,y )
    //
    // Parameters
    // x : a matrix of complex doubles
    // y : a matrix of complex doubles
    // z : a matrix of complex doubles, the complex division result z=x/y.
    // 
    // Description
    // This implementation uses the Smith's 1962 scaling
    // but with an expression of t=1/(1+r^2) which cannot overflow.
    // Uses a robust product, based on the frexp function.
    //
    // Examples
    // expected = 11/25 + %i * 2/25
    // compdiv_smith13 ( 1+2*%i , 3+4*%i )
    //
    // Authors
    // Copyright (C) 2010 - Robert L. Smith - Michael Baudin

    a = real(x)
    b = imag(x)
    c = real(y)
    d = imag(y)
    if ( abs(d) <= abs(c) ) then
        r = d/c
        t = 1/(1+r^2) // Cannot overflow
        e = (a/c) * t + compdiv_prodfrexp ( [b 1/c d 1/c t])
        f = (b/c) * t - compdiv_prodfrexp ( [a 1/c d 1/c t])
        // Treat the cases which produce Nans
        if ( isnan(e) ) then
            e = (a + b * r ) * t
        end
        if ( isnan(f) ) then
            f = (b - a * r ) * t
        end
    else
        r = c/d
        t = 1/(1+r^2)  // Cannot overflow
        e = compdiv_prodfrexp ( [a 1/d c 1/d t]) + (b/d) * t
        f = compdiv_prodfrexp ( [b 1/d c 1/d t]) - (a/d) * t
        // Treat the cases which produce Nans
        if ( isnan(e) ) then
            e = (a*r + b ) * t
        end
        if ( isnan(f) ) then
            f = (b*r - a ) * t
        end
    end
    // Avoid using x + %i * y, which may create
    // %inf+%i*%inf, which actually multiplies 0 and %inf,
    // generating an unwanted %nan.
    z = complex(e,f)
endfunction


