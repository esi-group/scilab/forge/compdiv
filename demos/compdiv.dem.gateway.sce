// Copyright (C) 2010-2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

demopath = get_absolute_file_path("compdiv.dem.gateway.sce");
subdemolist = [
"benchmark", "benchmark.sce"; ..
"benchmark_plot", "benchmark_plot.sce"; ..
"search_failure", "search_failure.sce"; ..
"difficult_cases", "difficult_cases.sce"; ..
"difficultcasesall", "difficultcasesall.sce"; ..
"complexdivision2", "complexdivision2.sce"; ..
"complexdivision", "complexdivision.sce"; ..
];
subdemolist(:,2) = demopath + subdemolist(:,2)
