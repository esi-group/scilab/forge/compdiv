// Copyright (C) 2010-2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

/* -------------------------------------------------------------------------- */
/* This gateway uses the new scilab api
/* -------------------------------------------------------------------------- */

#include "machine.h"
#include "Scierror.h"
#include "stack-c.h"
#include "api_scilab.h"
#include "localization.h"

#include "compdiv.h"

// z = compdiv_frobust(x,y) returns the complex division x/y by 
// Robust algorithm.
int sci_compdiv_frobust (char * fname) {

	SciErr sciErr;
	int *piAddr = NULL;
	int iType = 0;
	int rowsX,colsX;
	double* lrX = NULL;
	double* liX = NULL;
	int iComplex = 0;
	int rowsY,colsY;
	double* lrY = NULL;
	double* liY = NULL;
	int one=1;
	double* lrZ = NULL;
	double* liZ = NULL;
	double x[2];
	double y[2];
	double z[2];
	int i, j;

	int minlhs=1, minrhs=2, maxlhs=1, maxrhs=2;

	CheckRhs(minrhs,maxrhs) ;
	CheckLhs(minlhs,maxlhs) ;

	//
	// Read X
	//
	sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddr);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}
	sciErr = getVarType(pvApiCtx, piAddr, &iType);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}
	if ( iType != sci_matrix ) 
	{
		Scierror(999,_("%s: Wrong type for input argument #%d. Matrix expected.\n"),fname,1);
		return 0;
	}
	iComplex	= isVarComplex(pvApiCtx, piAddr);
	if ( iComplex == 0 ) 
	{
		Scierror(999,_("%s: Wrong content for input argument #%d. Complex matrix expected.\n"),fname,1);
		return 0;
	}
	sciErr = getComplexMatrixOfDouble(pvApiCtx, piAddr, &rowsX, &colsX, &lrX, &liX);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}
	//
	// Read Y
	//
	sciErr = getVarAddressFromPosition(pvApiCtx, 2, &piAddr);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}
	sciErr = getVarType(pvApiCtx, piAddr, &iType);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}
	if ( iType != sci_matrix ) 
	{
		Scierror(999,_("%s: Wrong type for input argument #%d. Matrix expected.\n"),fname,1);
		return 0;
	}
	iComplex	= isVarComplex(pvApiCtx, piAddr);
	if ( iComplex == 0 ) 
	{
		Scierror(999,_("%s: Wrong content for input argument #%d. Complex matrix expected.\n"),fname,1);
		return 0;
	}
	sciErr = getComplexMatrixOfDouble(pvApiCtx, piAddr, &rowsY, &colsY, &lrY, &liY);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}
	//
	// Check that x and y have the same size
	//
	if ( rowsX != rowsY | colsX != colsY ) 
	{
		Scierror(999,_("%s: Arguments #%d and #%d must have the same size.\n"),fname,1,2);
		return 0;
	}
	//
	// Create Z
	//
	sciErr = allocComplexMatrixOfDouble(pvApiCtx,Rhs+1, rowsX, colsX, &lrZ, &liZ);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}
	//
	// Perform the division
	//
	for(i = 0 ; i < rowsX ; i++)
	{
		for(j = 0 ; j < colsX ; j++)
		{
			x[0] = lrX[i + rowsX * j];
			x[1] = liX[i + rowsX * j];
			y[0] = lrY[i + rowsX * j];
			y[1] = liY[i + rowsX * j];
			compdiv_robust(x, y, z);
			lrZ[i + rowsX * j] = z[0];
			liZ[i + rowsX * j] = z[1];
		}
	}

	LhsVar(1) = Rhs+1;

	return(0);

}

