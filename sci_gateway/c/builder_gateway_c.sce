// ====================================================================
// Copyright (C) 2009 - 2011 - Michael Baudin
// This file is released into the public domain
// ====================================================================


gateway_path = get_absolute_file_path("builder_gateway_c.sce");

libname = "compdivgateway";
namelist = [
  "compdiv_priest"     "sci_compdiv_priest"
  "compdiv_fansiisoc"  "sci_compdiv_fansiisoc"
  "compdiv_fimproved"  "sci_compdiv_fimproved"
  "compdiv_frobust"    "sci_compdiv_frobust"
  "compdiv_fstewart"   "sci_compdiv_fstewart"
];
files = [
  "sci_compdiv_priest.c"
  "sci_compdiv_fansiisoc.c"
  "sci_compdiv_frobust.c"
  "sci_compdiv_fimproved.c"
  "sci_compdiv_fstewart.c"
  "gw_support.c"
  ];


ldflags = ""

if ( MSDOS ) then
  include2 = "..\..\src\c";
  include3 = SCI+"/modules/output_stream/includes";
  cflags = "-DWIN32 -I"""+include2+""" -I"""+include3+"""";
else
  include1 = gateway_path;
  include2 = gateway_path+"../../src/c";
  include3 = SCI+"/../../include/scilab/localization";
  include4 = SCI+"/../../include/scilab/output_stream";
  include5 = SCI+"/../../include/scilab/core";
  cflags = "-I"""+include1+""" -I"""+include2+""" -I"""+include3+...
    """ -I"""+include4+""" -I"""+include5+"""";
end
// Caution : the order matters !
libs = [
  "../../src/c/libcompdiv"
];

tbx_build_gateway(libname, namelist, files, gateway_path, libs, ldflags, cflags);

clear tbx_build_gateway;

