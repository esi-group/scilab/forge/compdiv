// Copyright (C) 2010-2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

expected = 11/25 + %i * 2/25;
z = compdiv_frobust ( 1+2*%i , 3+4*%i );
assert_checkalmostequal(z,expected);
//
// Test on larger dataset.
ieee(2);
path = compdiv_getpath();
dataset = fullfile(path,"tests","unit_tests","cdiv.dataset.csv");
dg = compdiv_testdataset ( dataset , compdiv_frobust );
assert_checktrue(mean(dg)>50);
//
// Test on difficult cases
ieee(2);
[x,y,z,q,digits] = compdiv_difficultcases(compdiv_frobust);
expected=[53,53,53,53,53,53,53,52,53,53,0,53,0,53,53]';
assert_checkequal(floor(digits),expected);
//
// Check that compdiv_robust and compdiv_frobust produce the same results.
// But frobust is compiled, and faster.
//
ieee(2);
path = compdiv_getpath();
dataset = fullfile(path,"tests","unit_tests","cdiv.dataset.csv");
digits1 = compdiv_testdataset ( dataset , compdiv_robust );
digits2 = compdiv_testdataset ( dataset , compdiv_frobust );
assert_checkequal(digits1,digits2);

