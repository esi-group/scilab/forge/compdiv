// Copyright (C) 2010 - 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


// <-- JVM NOT MANDATORY -->

// Some attempts to improve Smith's algorithm

ieee(2);

function [msg,digits] = fmtdig ( x , y , expected , divfunc , headermsg , footermsg )
    // Performs the complex division x/y, compare to expected, and produve a message 
    // containing the number of digits in the real and imaginary parts.
    r = divfunc ( a + %i * b , c + %i * d );
    digits = compdiv_computedigits(r,expectedR,basis);
    msg = msprintf("%s=%2d%s\n", headermsg , digits , footermsg )
endfunction

////////////////////////////////////////////////////////////////////////
// 
// Check accuracy
format("e",10);
path=compdiv_getpath (  );
basis = 2;
msg=[];
dg=[];
dataset = fullfile(path,"tests","unit_tests","cdiv.dataset.csv");
table = compdiv_datasetread ( dataset , "#" , "," , %t );
ntests = size(table,"r");
atable = evstr(table(:,1));
btable = evstr(table(:,2));
ctable = evstr(table(:,3));
dtable = evstr(table(:,4));
etable = evstr(table(:,5));
ftable = evstr(table(:,6));
for k = 1 : ntests
    a = atable(k);
    b = btable(k);
    c = ctable(k);
    d = dtable(k);
    e = etable(k);
    f = ftable(k);
    //
    expectedR = complex(e,f);
    x = complex(a,b);
    y = complex(c,d);
    msg(1) = msprintf("Test #%3d/%d, %70s, ",  k,ntests,sci2exp([a b c d e f]) );
    dg(k,1)=0;
    j=2;
    [msg(j),dg(k,j)] = fmtdig ( x , y , expectedR , compdiv_scilab , "Sc" , ", " );
    j=j+1;
    [msg(j),dg(k,j)] = fmtdig ( x , y , expectedR , compdiv_naive , "Na" , ", " );
    j=j+1;
    [msg(j),dg(k,j)] = fmtdig ( x , y , expectedR , compdiv_smith , "Sm" , ", " );
    j=j+1;
    [msg(j),dg(k,j)] = fmtdig ( x , y , expectedR , compdiv_stewart , "St" , ", " );
    j=j+1;
    [msg(j),dg(k,j)] = fmtdig ( x , y , expectedR , compdiv_improved , "IM" , ", " );
    j=j+1;
    [msg(j),dg(k,j)] = fmtdig ( x , y , expectedR , compdiv_ansiisoc , "AC", ", "  );
    j=j+1;
    [msg(j),dg(k,j)] = fmtdig ( x , y , expectedR , compdiv_smithLi , "Li" , ", " );
    j=j+1;
    [msg(j),dg(k,j)] = fmtdig ( x , y , expectedR , compdiv_priest , "PR" , ", " );
    j=j+1;
    [msg(j),dg(k,j)] = fmtdig ( x , y , expectedR , compdiv_smithR , "SR" , ", " );
    j=j+1;
    [msg(j),dg(k,j)] = fmtdig ( x , y , expectedR , compdiv_robust , "RO" , ", " );
    j=j+1;
    mprintf("%s\n",strcat(msg,""));
end
mprintf("Average:\n");
j=2;
mprintf("Sc=%d\n",mean(dg(:,j)));
j=j+1;
mprintf("Na=%d\n",mean(dg(:,j)));
j=j+1;
mprintf("Sm=%d\n",mean(dg(:,j)));
j=j+1;
mprintf("ST=%d\n",mean(dg(:,j)));
j=j+1;
mprintf("IM=%d\n",mean(dg(:,j)));
j=j+1;
mprintf("AC=%d\n",mean(dg(:,j)));
j=j+1;
mprintf("Li=%d\n",mean(dg(:,j)));
j=j+1;
mprintf("PR=%d\n",mean(dg(:,j)));
j=j+1;
mprintf("SR=%d\n",mean(dg(:,j)));
j=j+1;
mprintf("RO=%d\n",mean(dg(:,j)));
j=j+1;

// Discovered Issues:
// (0+%i*1 )/(0+%i*0)    Scilab=Nan+Nan i Octave=NaN+Inf i Matlab=NaN+Inf i
// (1+%i*1)/(0+%i*0)     Scilab=NaN+NaN i Octave=Inf+Inf i Matlab=Inf+Inf i
// (1+%i*-1)/(0+%i*0)    Scilab=Nan+Nan i Octave=Inf-Inf i Matlab=Inf-Inf i
// (%inf+%i*1)/(1+%i*1)  Scilab=NaN-Inf i Octave=Inf-Inf i Matlab=Inf-Inf i 
//
// smith3-improved Smith method, with an exp-log product-does not improve much.
// Li improves for some case, but not all.
// Only smith5 works in all the current test cases.
//

