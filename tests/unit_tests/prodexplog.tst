// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Basic tests
x = [1.e308 1.e308 1.e-308 1.e-308];
r = compdiv_prodexplog ( x );
assert_checkalmostequal(r,1,%eps);
//
p = compdiv_prodexplog ( [1 3 1 2] );
assert_checkalmostequal(p,6,%eps);
//
p = compdiv_prodexplog ( [-1 2 -3 4 1 -2 3 4] );
assert_checkalmostequal(p,-576,%eps);
//
p = compdiv_prodexplog ( [-1 1 1] );
assert_checkequal(p,-1);
//
p = compdiv_prodexplog ( [1 -1 1] );
assert_checkequal(p,-1);
//
p = compdiv_prodexplog ( [1.e200 1.e200 1.e-100] );
assert_checkalmostequal(p,1.e300,2^10*%eps);

// Accuracy test
x = [
-8207398118072301*2^-53
8727187368991500*2^-54
6237403295485065*2^-56
-6707097243752269*2^-55
-6829010895736168*2^-55
5265283919956334*2^-53
4521970942753754*2^-51
-6348554437180427*2^-53
7523931345950583*2^-55
-5372571516972004*2^-52
];
e = -0.277920000769307861379e-3;
r = compdiv_prodexplog(x);
assert_checkalmostequal(r,e,2^5*%eps);

// Random test
for k = 1 : 10
    x = rand(10,1,"normal");
    r = compdiv_prodexplog(x);
    e = prod(x);
    assert_checkalmostequal(r,e,2^5*%eps);
end

