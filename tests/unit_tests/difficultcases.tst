// Copyright (C) 2010-2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

//
ntests = 15;
//
x = compdiv_difficultcases();
[x,y] = compdiv_difficultcases();
[x,y,z] = compdiv_difficultcases();
assert_checkequal(size(x),[ntests 1]);
assert_checkequal(size(y),[ntests 1]);
assert_checkequal(size(z),[ntests 1]);
//
ieee(2);
x = compdiv_difficultcases(compdiv_naive);
[x,y] = compdiv_difficultcases(compdiv_naive);
[x,y,z] = compdiv_difficultcases(compdiv_naive);
[x,y,z,q] = compdiv_difficultcases(compdiv_naive);
[x,y,z,q,digits] = compdiv_difficultcases(compdiv_naive);
assert_checkequal(size(x),[ntests 1]);
assert_checkequal(size(y),[ntests 1]);
assert_checkequal(size(z),[ntests 1]);
assert_checkequal(size(q),[ntests 1]);
assert_checkequal(size(digits),[ntests 1]);
assert_checktrue(digits>=0);
assert_checktrue(digits<=53);
//
ieee(2);
[x,y,z,q,digits] = compdiv_difficultcases(compdiv_naive,%f);
assert_checkequal(size(x),[ntests 1]);
assert_checkequal(size(y),[ntests 1]);
assert_checkequal(size(z),[ntests 1]);
assert_checkequal(size(q),[ntests 1]);
assert_checkequal(size(digits),[ntests 1]);
assert_checktrue(digits>=0);
assert_checktrue(digits<=53);

