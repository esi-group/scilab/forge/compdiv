// Copyright (C) 2010-2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

x = [-5 -4 0 4 5];
y = [-1 0 1 2 3];
r = compdiv_copysign(x,y);
expected = [-5 4 0 4 5];
assert_checkequal(r,expected);

