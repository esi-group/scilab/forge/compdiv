// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Basic tests
x = [1.e308 1.e308 1.e-308 1.e-308];
r = compdiv_prodfrexp ( x );
assert_checkalmostequal(r,1,%eps);

// Accuracy test
x = [
-8207398118072301*2^-53
8727187368991500*2^-54
6237403295485065*2^-56
-6707097243752269*2^-55
-6829010895736168*2^-55
5265283919956334*2^-53
4521970942753754*2^-51
-6348554437180427*2^-53
7523931345950583*2^-55
-5372571516972004*2^-52
];
e = -0.277920000769307861379e-3;
r = compdiv_prodfrexp(x);
assert_checkalmostequal(r,e,%eps);

// Random test
for k = 1 : 10
    x = rand(10,1,"normal");
    r = compdiv_prodfrexp(x);
    e = prod(x);
    assert_checkalmostequal(r,e,%eps);
end
