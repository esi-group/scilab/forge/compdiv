// Copyright (C) 2008 - INRIA - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->

format("v",10);

//
dmax = -log10(2^(-53));
//
computed = compdiv_computedigits ( 1 , 1 );
assert_checkequal ( computed , dmax );
//
computed = compdiv_computedigits ( 0 , 0 );
assert_checkequal ( computed , dmax );
//
computed = compdiv_computedigits ( 1 , 0 );
assert_checkequal ( computed , 0 );
//
computed = compdiv_computedigits ( 0 , 1 );
assert_checkequal ( computed , 0 );
//
computed = compdiv_computedigits ( 3.1415926 , %pi );
assert_checkequal ( computed , 7.76806779280040160 );
//
computed = compdiv_computedigits ( 3.1415926 , %pi , 2 );
assert_checkequal ( computed , 25.80496264389331884 );
//
computed = compdiv_computedigits ( [0 0 1 1] , [0 1 0 1] );
assert_checkequal ( computed , [dmax 0 0 dmax] );
//
computed = compdiv_computedigits(ones(3,2),ones(3,2));
assert_checkequal ( computed , dmax * ones(3,2) );
//
computed = compdiv_computedigits([%nan %nan %nan %nan],[%nan %inf -%inf 0]);
assert_checkequal ( computed , [dmax 0 0 0] );
//
computed = compdiv_computedigits([%inf %inf %inf %inf],[%nan %inf -%inf 0]);
assert_checkequal ( computed , [0 dmax 0 0] );
//
computed = compdiv_computedigits([-%inf -%inf -%inf -%inf],[%nan %inf -%inf 0]);
assert_checkequal ( computed , [0 0 dmax 0] );
//
computed = compdiv_computedigits([0 0 0 0],[%nan %inf -%inf 0]);
assert_checkequal ( computed , [0 0 0 dmax] );
//
computed = compdiv_computedigits(1.224646799D-16,8.462643383D-18);
assert_checkequal ( computed , 0 );
//
computed = compdiv_computedigits ( 1.2345 + %i*6.7891 , 1.23456789 + %i*6.789123456 );
assert_checkequal ( computed , 4.259709168495138698 );
//
// The sign bit of the number of digits may be wrong because
// ieee(2); z=max(-0,0); 1/z is -%inf
back = ieee();
ieee(2);
computed = compdiv_computedigits ( 1.e-305 , 0 );
assert_checkequal ( 1/computed , %inf );
//
computed = compdiv_computedigits ( 0 , 1.e-305 );
assert_checkequal ( 1/computed , %inf );
ieee(back);
//
d = compdiv_computedigits([%inf 1],[1 %inf]);
assert_checkequal ( d , [0 0]);
//
// An empirically found test case
a = [
3.982729777831130693D-59
2.584939414228211484D-26
4.391531370352049090D+43 
1.725436586898508346D+68
];
b = [
3.982729777831130693D-59
2.584939414228211484D-26
4.391531370352048595D+43
1.725436586898508107D+68
];
c = compdiv_computedigits ( a , b , 2 );
e = [
    53.        
    53.        
    52.977632  
    52.678072  
];
assert_checkalmostequal ( c , e , 1.e-7 );


