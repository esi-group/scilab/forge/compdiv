// Copyright (C) 2010 - 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// <-- JVM NOT MANDATORY -->
ieee(2);
stacksize("max");
defk = 1000;
// With default parameters
grand("setsd",0);
[j , p, s, k] = compdiv_searchfordiff ( compdiv_naive , compdiv_scilab );
k=100, j=52, p=5.200e-01, 95%=[4.22e-01,6.18e-01]
k=200, j=101, p=5.050e-01, 95%=[4.36e-01,5.74e-01]
k=300, j=146, p=4.867e-01, 95%=[4.30e-01,5.43e-01]
k=400, j=202, p=5.050e-01, 95%=[4.56e-01,5.54e-01]
k=500, j=246, p=4.920e-01, 95%=[4.48e-01,5.36e-01]
k=600, j=287, p=4.783e-01, 95%=[4.38e-01,5.18e-01]
k=700, j=337, p=4.814e-01, 95%=[4.44e-01,5.18e-01]
k=800, j=390, p=4.875e-01, 95%=[4.53e-01,5.22e-01]
k=900, j=435, p=4.833e-01, 95%=[4.51e-01,5.16e-01]
k=1000, j=475, p=4.750e-01, 95%=[4.44e-01,5.06e-01]
assert_checktrue(j>0.3*defk);
assert_checktrue(p>0.3);
assert_checkalmostequal(s,sqrt(0.49*(1-0.49)/defk),1.e-2);
assert_checkequal(k,defk);
//
// With verbose mode
grand("setsd",0);
[j , p, s, k] = compdiv_searchfordiff ( compdiv_naive , compdiv_scilab , [],10,[],[],1);
#3 (1): (2^  589+%i*2^ -286) op (2^  993+%i*2^   69), q1=(         Nan,  +0.000e+00), q2=( -2.420e-122,  +0.000e+00), d=0.00
#4 (2): (2^ -300+%i*2^  133) op (2^  870+%i*2^  692), q1=(  +0.000e+00,  -0.000e+00), q2=( -3.610e-276, -1.383e-222), d=0.00
#7 (3): (2^  391+%i*2^  248) op (2^  405+%i*2^  788), q1=(         Nan,         Nan), q2=( +2.778e-163, -3.098e-120), d=0.00
#8 (4): (2^ -631+%i*2^ -258) op (2^ -611+%i*2^ -631), q1=(         Inf,         Inf), q2=( +1.750e+100, +1.835e+106), d=0.00
#9 (5): (2^ -389+%i*2^  660) op (2^ -979+%i*2^  365), q1=(        -Inf, +1.055e-227), q2=(  -6.366e+88, +1.055e-227), d=0.00
End of the algorithm.
k=10, j=5, p=5.000e-01, 95%=[1.90e-01,8.10e-01]
assert_checkequal(k,10);
//
// With customized parameters
grand("setsd",0);
nmatrix = [
-1074:1023
-1074:1023
-1074:1023
-1074:1023
];
kmax = 100000;
rtol = 1.e-1;
atol = 0;
verbose = 0;
compdiv_searchfordiff ( compdiv_naive , compdiv_scilab , nmatrix , kmax , rtol , atol , verbose );
k=100, j=48, p=4.800e-01, 95%=[3.82e-01,5.78e-01]
k=200, j=97, p=4.850e-01, 95%=[4.16e-01,5.54e-01]
k=300, j=146, p=4.867e-01, 95%=[4.30e-01,5.43e-01]
k=400, j=183, p=4.575e-01, 95%=[4.09e-01,5.06e-01]
k=500, j=231, p=4.620e-01, 95%=[4.18e-01,5.06e-01]
k=600, j=279, p=4.650e-01, 95%=[4.25e-01,5.05e-01]
k=700, j=328, p=4.686e-01, 95%=[4.32e-01,5.06e-01]
k=800, j=382, p=4.775e-01, 95%=[4.43e-01,5.12e-01]
k=900, j=436, p=4.844e-01, 95%=[4.52e-01,5.17e-01]
k=1000, j=486, p=4.860e-01, 95%=[4.55e-01,5.17e-01]
k=1100, j=538, p=4.891e-01, 95%=[4.60e-01,5.19e-01]
k=1200, j=592, p=4.933e-01, 95%=[4.65e-01,5.22e-01]
k=1300, j=645, p=4.962e-01, 95%=[4.69e-01,5.23e-01]
k=1400, j=688, p=4.914e-01, 95%=[4.65e-01,5.18e-01]
// Display the worst cases
grand("setsd",0);
verbose = 2;
compdiv_searchfordiff ( compdiv_naive , compdiv_scilab , nmatrix , 10 , rtol , atol , verbose );
#3 (1): (2^  589+%i*2^ -286) op (2^  993+%i*2^   69), q1=(         Nan,  +0.000e+00), q2=( -2.420e-122,  +0.000e+00), d=0.00
#4 (2): (2^ -300+%i*2^  133) op (2^  870+%i*2^  692), q1=(  +0.000e+00,  -0.000e+00), q2=( -3.610e-276, -1.383e-222), d=0.00
#7 (3): (2^  391+%i*2^  248) op (2^  405+%i*2^  788), q1=(         Nan,         Nan), q2=( +2.778e-163, -3.098e-120), d=0.00
#8 (4): (2^ -631+%i*2^ -258) op (2^ -611+%i*2^ -631), q1=(         Inf,         Inf), q2=( +1.750e+100, +1.835e+106), d=0.00
#9 (5): (2^ -389+%i*2^  660) op (2^ -979+%i*2^  365), q1=(        -Inf, +1.055e-227), q2=(  -6.366e+88, +1.055e-227), d=0.00
End of the algorithm.
k=10, j=5, p=5.000e-01, 95%=[1.90e-01,8.10e-01]
// Display all
grand("setsd",0);
verbose = 3;
compdiv_searchfordiff ( compdiv_naive , compdiv_scilab , nmatrix , 10 , rtol , atol , verbose );
#3 (1): (2^  589+%i*2^ -286) op (2^  993+%i*2^   69), q1=(         Nan,  +0.000e+00), q2=( -2.420e-122,  +0.000e+00), d=0.00
#4 (2): (2^ -300+%i*2^  133) op (2^  870+%i*2^  692), q1=(  +0.000e+00,  -0.000e+00), q2=( -3.610e-276, -1.383e-222), d=0.00
#7 (3): (2^  391+%i*2^  248) op (2^  405+%i*2^  788), q1=(         Nan,         Nan), q2=( +2.778e-163, -3.098e-120), d=0.00
#8 (4): (2^ -631+%i*2^ -258) op (2^ -611+%i*2^ -631), q1=(         Inf,         Inf), q2=( +1.750e+100, +1.835e+106), d=0.00
#9 (5): (2^ -389+%i*2^  660) op (2^ -979+%i*2^  365), q1=(        -Inf, +1.055e-227), q2=(  -6.366e+88, +1.055e-227), d=0.00
End of the algorithm.
k=10, j=5, p=5.000e-01, 95%=[1.90e-01,8.10e-01]
// Try much harder cases.
grand("setsd",0);
nmatrix = [-1074:-900 900:1023; -1074:-900 900:1023; -1074:-900 900:1023; -1074:-900 900:1023 ];
verbose = 2;
compdiv_searchfordiff ( compdiv_naive , compdiv_scilab , nmatrix , 10 , rtol , atol , verbose );
#3 (1): (2^-1047+%i*2^  975) op (2^  994+%i*2^ 1013), q1=(         Nan,         Nan), q2=(  -3.638e-12,  +6.939e-18), d=0.00
#4 (2): (2^  988+%i*2^  941) op (2^  981+%i*2^-1031), q1=(         Nan,         Nan), q2=(  -1.280e+02,  -9.095e-13), d=0.00
#6 (3): (2^  911+%i*2^-1048) op (2^-1000+%i*2^ 1006), q1=(  +0.000e+00,         Nan), q2=(  -0.000e+00,  -2.524e-29), d=0.00
#7 (4): (2^-1006+%i*2^-1043) op (2^ -929+%i*2^ -967), q1=(         Nan,         Nan), q2=(  -6.617e-24,  -7.222e-35), d=0.00
#9 (5): (2^ -914+%i*2^ -958) op (2^-1032+%i*2^ -927), q1=(         Nan,         Nan), q2=(  -4.657e-10,  +8.192e+03), d=0.00
#10 (6): (2^  950+%i*2^ -950) op (2^  911+%i*2^ -971), q1=(         Nan,  +0.000e+00), q2=(  -5.498e+11,  +0.000e+00), d=0.00
End of the algorithm.
k=10, j=6, p=5.000e-01, 95%=[1.90e-01,8.10e-01]
grand("setsd",0);
compdiv_searchfordiff ( compdiv_smith , compdiv_scilab , nmatrix , 10 , rtol , atol , verbose );
End of the algorithm.
k=10, j=0, p=0.000e+00, 95%=[0.00e+00,0.00e+00]
// To explore the larger exponents:
nmatrix = [ 900:1023; 900:1023; 900:1023; 900:1023 ];
// To explore the smaller exponents:
nmatrix = [ -1074:-900; -1074:-900; -1074:-900; -1074:-900 ];
// Customize the verbose function
function tf=myverbose(i, k, x, y, q1, q2, p, s, state )
    tf = %f
    if (state=="verb") then
        if ( modulo(k,1000)==0 ) then
        pmin = p-1.96*s
        pmax = p+1.96*s
        mprintf("k=%d, j=%d, p=%.3e, 95%%=[%.2e,%.2e]\n",k,j,p,pmin,pmax);
        end
    elseif ( state=="diff" ) then
        na = int(log2(real(x)))
        nb = int(log2(imag(x)))
        nc = int(log2(real(y)))
        nd = int(log2(imag(y)))
        mprintf("Difference identified\n");
        mprintf("(2^%d+%%i*2^%d)/(2^%d+%%i*2^%d)\n",na,nb,nc,nd);
        mprintf("  Div1=%s+%%i*%s\n",string(real(q1)),string(imag(q1)))
        mprintf("  Div2=%s+%%i*%s\n",string(real(q2)),string(imag(q2)))
    end
endfunction
compdiv_searchfordiff ( compdiv_naive , compdiv_scilab , nmatrix , 10 , rtol , atol , myverbose );
k=0, j=0, p=0.000e+00, 95%=[0.00e+00,0.00e+00]
Difference identified
(2^-917+%i*2^-1039)/(2^-994+%i*2^-1049)
  Div1=Nan+%i*Nan
  Div2=1.511D+23+%i*4194304
Difference identified
(2^-1005+%i*2^-966)/(2^-978+%i*2^-1065)
  Div1=Nan+%i*Nan
  Div2=7.451D-09+%i*4096
Difference identified
(2^-1055+%i*2^-927)/(2^-1018+%i*2^-1040)
  Div1=Nan+%i*Nan
  Div2=-5.903D+20+%i*-2.476D+27
Difference identified
(2^-966+%i*2^-966)/(2^-909+%i*2^-926)
  Div1=Nan+%i*Nan
  Div2=-6.939D-18+%i*6.939D-18
Difference identified
(2^-1005+%i*2^-964)/(2^-1065+%i*2^-1030)
  Div1=Nan+%i*Nan
  Div2=7.379D+19+%i*-2.114D+09
Difference identified
(2^-911+%i*2^-1008)/(2^-949+%i*2^-965)
  Div1=Nan+%i*Nan
  Div2=2.749D+11+%i*4194304
Difference identified
(2^-940+%i*2^-903)/(2^-1066+%i*2^-980)
  Div1=Nan+%i*Nan
  Div2=-1.511D+23+%i*-1.100D+12
Difference identified
(2^-1041+%i*2^-964)/(2^-978+%i*2^-1014)
  Div1=Nan+%i*Nan
  Div2=-0.0000002+%i*-16384
Difference identified
(2^-980+%i*2^-972)/(2^-1002+%i*2^-967)
  Div1=Nan+%i*Nan
  Div2=0.03125+%i*0.0001221
Difference identified
(2^-996+%i*2^-912)/(2^-966+%i*2^-910)
  Div1=Nan+%i*Nan
  Div2=-0.25+%i*-3.469D-18
// A verbose function with one line by message
function tf=myverbosebrief(i, k, x, y, q1, q2, p, s, state )
    function s=myformatd(d)
        if ( isnan(d) | isinf(d) ) then
            s = msprintf("%10s",string(d))
        else
            s = msprintf("%+.3e",d)
        end
    endfunction
    function s=myformatcomp(d)
        s = msprintf("(%12s,%12s)",myformatd(real(d)),myformatd(imag(d)))
    endfunction
    tf = %f
    if (state=="verb") then
        if ( modulo(k,1000)==0 ) then
        pmin = p-1.96*s
        pmax = p+1.96*s
        mprintf("k=%d, j=%d, p=%.3e, 95%%=[%.2e,%.2e]\n",k,j,p,pmin,pmax);
        end
    elseif ( state=="diff" ) then
        na = int(log2(real(x)))
        nb = int(log2(imag(x)))
        nc = int(log2(real(y)))
        nd = int(log2(imag(y)))
        mprintf("#%d (%d): (2^%5d+%%i*2^%5d)/(2^%5d+%%i*2^%5d), q1=%s, q2=%s\n", ..
        k,i,na,nb,nc,nd,..
        myformatcomp(q1),myformatcomp(q2));
    end
endfunction
grand("setsd",0);
compdiv_searchfordiff ( compdiv_naive , compdiv_scilab , nmatrix , 10 , rtol , atol , myverbosebrief );
k=0, j=0, p=0.000e+00, 95%=[0.00e+00,0.00e+00]
#1 (1): (2^ -930+%i*2^-1023)/(2^ -998+%i*2^ -968), q1=(         Nan,         Nan), q2=(  -2.560e+02,  +2.749e+11)
#2 (2): (2^ -910+%i*2^ -983)/(2^-1030+%i*2^ -962), q1=(         Nan,         Nan), q2=(  -1.478e-05,  -4.504e+15)
#3 (3): (2^ -941+%i*2^ -968)/(2^ -958+%i*2^ -986), q1=(         Nan,         Nan), q2=(  -1.311e+05,  +4.883e-04)
#4 (4): (2^ -939+%i*2^ -950)/(2^ -995+%i*2^ -964), q1=(         Nan,         Nan), q2=(  -1.638e+04,  +3.355e+07)
#5 (5): (2^ -961+%i*2^ -946)/(2^-1000+%i*2^-1029), q1=(         Nan,         Nan), q2=(  +5.497e+11,  +1.801e+16)
#6 (6): (2^ -995+%i*2^ -923)/(2^ -958+%i*2^-1014), q1=(         Nan,         Nan), q2=(  -4.768e-07,  +3.436e+10)
#7 (7): (2^-1047+%i*2^ -993)/(2^ -905+%i*2^ -952), q1=(         Nan,         Nan), q2=(  +2.278e-41,  -3.231e-27)
#8 (8): (2^-1071+%i*2^ -981)/(2^ -980+%i*2^ -925), q1=(         Nan,         Nan), q2=(  +1.388e-17,  +3.852e-34)
#9 (9): (2^ -902+%i*2^ -930)/(2^ -975+%i*2^-1038), q1=(         Nan,         Nan), q2=(  +9.445e+21,  +3.518e+13)
#10 (10): (2^-1041+%i*2^-1016)/(2^ -944+%i*2^ -986), q1=(         Nan,         Nan), q2=(  -6.311e-30,  -2.118e-22)
// By block
grand("setsd",0);
[j , p, s, k] = compdiv_searchfordiff ( compdiv_naive , compdiv_scilab , [] , [] , [] , [] , [] , %t );
assert_checktrue(j>0.3*defk);
assert_checktrue(p>0.3);
assert_checkalmostequal(s,sqrt(0.49*(1-0.49)/defk),1.e-2);
assert_checkequal(k,defk);
