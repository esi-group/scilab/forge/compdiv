// Copyright (C) 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function r = compdiv_priest ( x, y )
    // Performs fast complex division with Priest's algorithm.
    //
    // Calling Sequence
    // r = compdiv_priest ( x,y )
    //
    // Parameters
    // x : a matrix of complex doubles
    // y : a matrix of complex doubles
    // r : a matrix of complex doubles, the complex division result r=x/y.
    // 
    // Description
    // This implementation is based on Priest's algorithm and Priest's 
    // C function.
    // This is a fast implementation, based on compiled source code.
    // Scales by S almost equal to abs(c+i*d)^(-3/4).
    //
    // Compiling options are used to avoid the use of extended precision, and 
    // to strictly use SSE units, which results in predictable results.
    //
    // Examples
    // expected = 11/25 + %i * 2/25
    // compdiv_priest ( 1+2*%i , 3+4*%i )
    //
    // x = rand(3,5)+%i*rand(3,5)
    // y = rand(3,5)+%i*rand(3,5)
    // r = compdiv_priest ( x , y )
    // abs(r.*y - x)./abs(x)
    //
    // Authors
    // Copyright (C) 2010 - Michael Baudin
    // Copyright (C) 2004 - Douglas M. Priest, Sun Microsystems
    //
    // Bibliography
    // "Efficient scaling for complex division", Douglas M. Priest, Sun Microsystems, ACM Transactions on Mathematical software, Vol. 30, No. 4, December 2004

endfunction

