// Copyright (C) 2010 - 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function r = compdiv_fstewart ( x, y )
    // Performs fast complex division with Stewart's algorithm.
    //
    // Calling Sequence
    // z = compdiv_fstewart (x,y)
    //
    // Parameters
    // x : a matrix of complex doubles
    // y : a matrix of complex doubles
    // z : a matrix of complex doubles, the complex division result z=x/y.
    //
    // Description
    // This algorithms uses Smith's method but performs robust multiplications a*b*c,
    // avoiding unnecessary overflows.
    // This is a fast implementation, based on compiled source code.
    //
    // Examples
    // expected = 11/25 + %i * 2/25
    // compdiv_fstewart ( 1+2*%i , 3+4*%i )
    //
    // x = rand(3,5)+%i*rand(3,5)
    // y = rand(3,5)+%i*rand(3,5)
    // r = compdiv_fstewart ( x , y )
    // abs(r.*y - x)./abs(x)
    //
    // Bibliography
    //  "A Note on Complex Division", G. W. Stewart, University of Maryland, ACM Transactions on Mathematical Software, Vol. 11, No 3, September 1985, Pages 238-241
    //
    // Corrigendum: "A Note on Complex Division",G. W. Stewart, TOMS, Volume 12, Number 3, Pages = 285--285, September, 1986
    //
    // Authors
    // Copyright (C) 2010-2011 - Michael Baudin

endfunction

