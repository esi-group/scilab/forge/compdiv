// Copyright (C) 2010 - 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Updates the .xml files by deleting existing files and 
// creating them again from the .sci with help_from_sci.


//
cwd = get_absolute_file_path("update_help.sce");
mprintf("Working dir = %s\n",cwd);
//
// Generate the library help
mprintf("Updating compdiv\n");
helpdir = cwd;
funmat = [
 // "compdiv_scilab" // See bug http://forge.scilab.org/index.php/p/helptbx/issues/342/
  "compdiv_ansiisoc"
  "compdiv_naive"
  "compdiv_polar"
  "compdiv_smith"
  "compdiv_smithSZ"
  "compdiv_improved"
  "compdiv_robust"
  "compdiv_stewart"
  "compdiv_smithLi"
  "compdiv_smithR"
  ];
macrosdir = fullfile(cwd,"..","..","macros");
demosdir = [];
modulename = "compdiv";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );
//
// Generate the library help
mprintf("Updating compdiv/support\n");
helpdir = fullfile(cwd,"support");
funmat = [
  "compdiv_isfinite"
  "compdiv_pow2"
  "compdiv_copysign"
  "compdiv_hypot"
  "compdiv_searchfordiff"
  "compdiv_evaluate"
  "compdiv_difficultcases"
  "compdiv_prodfrexp"
  "compdiv_prodexplog"
  "compdiv_prodminmax"
  "compdiv_tryevaluate"
  "compdiv_maclaren"
  "compdiv_extreme"
  "compdiv_testdataset"
  "compdiv_storezero"
  "compdiv_testskahan"
  ];
macrosdir = fullfile(cwd,"..","..","macros");
demosdir = [];
modulename = "compdiv";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );
//
// Generate the gateway help
mprintf("Updating compdiv/gateway\n");
helpdir = cwd;
funmat = [
  "compdiv_priest"
  "compdiv_fimproved"
  "compdiv_frobust"
  "compdiv_fansiisoc"
  "compdiv_fstewart"
  ];
macrosdir = fullfile(cwd,"pseudomacros");
demosdir = [];
modulename = "compdiv";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );

//
// Generate the multiplications help
mprintf("Updating compdiv/multiplications\n");
helpdir = fullfile(cwd,"product");
funmat = [
  "compdiv_mulnaive"
  "compdiv_mulansic"
  "compdiv_mulscilab"
  ];
macrosdir = fullfile(cwd,"..","..","macros");
demosdir = [];
modulename = "compdiv";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );


