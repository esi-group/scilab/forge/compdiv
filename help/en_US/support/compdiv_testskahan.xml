<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from compdiv_testskahan.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="compdiv_testskahan" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">


  <refnamediv>
    <refname>compdiv_testskahan</refname><refpurpose>Test Kahan's complex divisions.</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   [x,y,z,digits] = compdiv_testskahan ( div )
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>div :</term>
      <listitem><para> a function, the complex division under test. The function must have the header r=div(x,y).</para></listitem></varlistentry>
   <varlistentry><term>x :</term>
      <listitem><para> a n-by-1 matrix of complex doubles, the numerator</para></listitem></varlistentry>
   <varlistentry><term>y :</term>
      <listitem><para> a n-by-1 matrix of complex doubles, the denominator</para></listitem></varlistentry>
   <varlistentry><term>z :</term>
      <listitem><para> a n-by-1 matrix of complex doubles, the exact complex division z=x./y</para></listitem></varlistentry>
   <varlistentry><term>digits :</term>
      <listitem><para> a n-by-1 matrix of doubles, the number of binary digits for the tests.</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
This function tests the accuracy of the complex division function <literal>div</literal>
based on a special set of divisions.
These extreme divisions are inspired by Kahan in "Marketing versus Mathematics",
in the Appendix:  Over/Underﬂow Undermines Complex Number Division in  Java.
   </para>
   <para>
The text by Kahan is:
"[Smith's formula] runs slower because of a third division;
it still generates spurious over/underflows at extreme
arguments; and it spoils users’ test cases when z should ideally
be real or pure imaginary ([note from me: this is case A]), or when
z, y and x = z·y are all small complex integers, but the computed z isn’t
([note from me: this is case B])."
   </para>
   <para>
In the current test set, there are n=5 tests.
The tests #1 and #2 are in case A and tests #3 to 5 are in case B.
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
format("e",25)
[x,y,z,digits] = compdiv_testskahan ( compdiv_naive )
compdiv_naive(x,y)

// Test smith
[x,y,z,digits] = compdiv_testskahan ( compdiv_smith )
compdiv_smith(x,y)

   ]]></programlisting>
</refsection>

<refsection>
   <title>Bibliography</title>
   <para>"Marketing versus Mathematics, and other Ruminations on the Design of Floating-Point Arithmetic", W. Kahan, August 27, 2000"</para>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Copyright (C) 2011 - Michael Baudin</member>
   </simplelist>
</refsection>
</refentry>
