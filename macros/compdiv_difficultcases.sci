// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [x,y,z,q,digits] = compdiv_difficultcases(varargin)
    // A small collection of difficult complex divisions.
    //
    // Calling Sequence
    // x = compdiv_difficultcases()
    // x = compdiv_difficultcases(div)
    // x = compdiv_difficultcases(div,verbose)
    // [x,y] = compdiv_difficultcases(...)
    // [x,y,z] = compdiv_difficultcases(...)
    // [x,y,z,q] = compdiv_difficultcases(div)
    // [x,y,z,q] = compdiv_difficultcases(div,verbose)
    // [x,y,z,q,digits] = compdiv_difficultcases(div)
    // [x,y,z,q,digits] = compdiv_difficultcases(div,verbose)
    //
    // Parameters
    // div : a function, the complex division under test. The function must have the header r=div(x,y).
    // verbose : a 1-by-1 matrix of booleans, set to true to print messages (default = %t).
    // digits : a 10-by-1 matrix of doubles, positive integers, the number of significant digits for each case.
    // x : a 10-by-1 matrix of doubles, positive integers, the numerator
    // y : a 10-by-1 matrix of doubles, positive integers, the denominator
    // z : a 10-by-1 matrix of doubles, positive integers, the expected x/y
    // q : a 10-by-1 matrix of doubles, positive integers, the computed x/y
    // 
    // Description
    // A small collection of difficult complex divisions.
    // The goal of this collection is to have a fast way 
    // of evaluating a complex division algorithm.
    //
    // Examples
    // [x,y,z] = compdiv_difficultcases()
    // [x,y,z,q,digits] = compdiv_difficultcases(compdiv_improved);
    //
    // // The cases are :
    // // Case 1
    // x = 1 + %i;
    // y = 1 + %i * 2^1023;
    // z = 2^-1023 - %i * 2^-1023;
    // // Case 2
    // x = 1 + %i;
    // y = 2^-1023 + %i * 2^-1023;
    // z = 2^1023;
    // // Case 3
    // x = 2^1023 + %i * 2^-1023;
    // y = 2^677 + %i * 2^-677;
    // z = 2^346 - %i * 2^-1008;
    // // Case 4
    // x = 2^1023+%i*2^1023;
    // y = 1+%i;
    // z = 2^1023;
    // // Case 5
    // x = 2^1020 + %i * 2^-844;
    // y = 2^656 + %i * 2^-780;
    // z = 2^364 - %i * 2^-1072;
    // // Case 6
    // x = 2^-71 + %i*2^1021;
    // y = 2^1001 + %i * 2^-323;
    // z = 2^-1072 + %i * 2^20;
    // // Case 7
    // x = 2^-347+%i*2^-54;
    // y = 2^-1037+%i*2^-1058;
    // z = 3.898125604559113300e289 + %i * 8.174961907852353577e295;
    // // Case 8
    // x = 2^-1074+%i*2^-1074;
    // y = 2^-1073+%i*2^-1074;
    // z=0.6+%i*0.2;
    // // Case 9
    // x = 2^1015+%i*2^-989;
    // y = 2^1023+%i*2^1023;
    // z = 0.001953125-%i*0.001953125;
    // // Case 10
    // x = 2^-622+%i*2^-1071;
    // y = 2^-343+%i*2^-798;
    // z = 1.02951151789360578e-84 + %i * 6.97145987515076231e-220;
    // // Case 11
    // x = 2^-912+%i*2^-1029;
    // y = 2^-122+%i*2^46;
    // z = 2^-1074+%i*-2^-958;
    // // Case 12, James Demmel, Personal communication, 2011
    // UN = number_properties("tiny");
    // x = 2*UN + %i * 2.5*UN;
    // y = -UN + %i * UN;
    // z = 0.25 - %i * 2.25;
    // // Case 13, Variant of case 12
    // UN = number_properties("tiniest");
    // x = 2*UN + %i * 2.5*UN;
    // y = -UN + %i * UN;
    // z = 0.25 - %i * 2.25;
    // // Case 14, Underflow and the reliability of numerical software, James Demmel, 1984
    // UN = number_properties("tiny");
    // x = 2*UN + %i*UN;
    // y = 4*UN + %i*2*UN;
    // z = 0.5;
    // // Case 15, Underflow and the reliability of numerical software, James Demmel, 1984
    // UN = number_properties("tiniest");
    // x = 2*UN + %i*UN;
    // y = 4*UN + %i*2*UN;
    // z = 0.5;
    //
    // Authors
    // Copyright (C) 2011 - Michael Baudin

    function msg = cd_compare(r,z)
        if ( r <> z ) then
            msg = "failed"
        else
            msg = "OK"
        end
    endfunction
    function [x,y,z] = testcases()
        k=0
        // Case 1
        k=k+1
        x(k) = 1 + %i;
        y(k) = 1 + %i * 2^1023;
        z(k) = 2^-1023 - %i * 2^-1023;

        // Case 2
        k=k+1
        x(k) = 1 + %i;
        y(k) = 2^-1023 + %i * 2^-1023;
        z(k) = 2^1023;

        // Case 3
        k=k+1
        x(k) = 2^1023 + %i * 2^-1023;
        y(k) = 2^677 + %i * 2^-677;
        z(k) = 2^346 - %i * 2^-1008;

        // Case 4
        k=k+1
        x(k) = 2^1023+%i*2^1023;
        y(k) = 1+%i;
        z(k) = 2^1023;

        // Case 5
        k=k+1
        x(k) = 2^1020 + %i * 2^-844;
        y(k) = 2^656 + %i * 2^-780;
        z(k) = 2^364 - %i * 2^-1072;

        // Case 6
        k=k+1
        x(k) = 2^-71 + %i*2^1021;
        y(k) = 2^1001 + %i * 2^-323;
        z(k) = 2^-1072 + %i * 2^20;

        // Case 7
        k=k+1
        x(k) = 2^-347+%i*2^-54;
        y(k) = 2^-1037+%i*2^-1058;
        z(k) = 3.898125604559113300e289 + %i * 8.174961907852353577e295;

        // Case 8
        k=k+1
        x(k) = 2^-1074+%i*2^-1074;
        y(k) = 2^-1073+%i*2^-1074;
        z(k)=0.6+%i*0.2;

        // Case 9
        k=k+1
        x(k) = 2^1015+%i*2^-989;
        y(k) = 2^1023+%i*2^1023;
        z(k) = 0.001953125-%i*0.001953125;

        // Case 10
        k=k+1
        x(k)=2^-622+%i*2^-1071;
        y(k)=2^-343+%i*2^-798;
        z(k)=1.02951151789360578e-84 + %i * 6.97145987515076231e-220;

        // Case 11
        k = k+1
        x(k)=2^-912+%i*2^-1029;
        y(k)=2^-122+%i*2^46;
        z(k)=2^-1074+%i*-2^-958;

        // Case 12, James Demmel, Personal Communication, 2011
        k = k+1
        UN = number_properties("tiny");
        x(k) = 2*UN + %i * 2.5*UN;
        y(k) = -UN + %i * UN;
        z(k) = 0.25 - %i * 2.25;

        // Case 13, Variant of case 12
        k = k+1
        UN = number_properties("tiniest");
        x(k) = 2*UN + %i * 2.5*UN;
        y(k) = -UN + %i * UN;
        z(k) = 0.25 - %i * 2.25;

        // Case 14, Underflow and the reliability of numerical software, James Demmel, 1984
        k = k+1
        UN = number_properties("tiny");
        x(k) = 2*UN + %i*UN;
        y(k) = 4*UN + %i*2*UN;
        z(k) = 0.5;

        // Case 15, Underflow and the reliability of numerical software, James Demmel, 1984
        k = k+1
        UN = number_properties("tiniest");
        x(k) = 2*UN + %i*UN;
        y(k) = 4*UN + %i*2*UN;
        z(k) = 0.5;
    endfunction

    [lhs, rhs] = argn()
    apifun_checkrhs ( "compdiv_difficultcases" , rhs , 0:2 )
    apifun_checklhs ( "compdiv_difficultcases" , lhs , 0:5 )
    //
    //
    [x,y,z] = testcases()
    //
    if ( rhs== 0 ) then
        apifun_checklhs ( "compdiv_difficultcases" , lhs , 0:3 )
        return
    end
    //
    div = varargin ( 1 )
    if ( rhs<= 1 ) then
        verbose = %t
    else
        verbose = varargin(2)
    end
    //
    ntests = size(x,"*")
    for k = 1 : ntests
        q(k) = div(x(k),y(k))
        msg = cd_compare(q(k),z(k))
        digits(k) = compdiv_computedigits(q(k),z(k),2)
        if ( verbose ) then
        mprintf("Case %d: %-6s (%.1f bits)\n",k,msg,digits(k));
        end
    end
endfunction


