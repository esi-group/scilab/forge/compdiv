// Copyright (C) 2010-2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function p = compdiv_prodfrexp ( x )
    //   Returns the product of the entries in x.
    //
    // Calling Sequence
    // p = compdiv_prodfrexp ( x )
    //
    // Parameters
    // x : a matrix of doubles
    // p : a 1-by-1 matrix of doubles, the product x(1)*x(2)*...*x(n)
    //
    // Description
    // Uses the frexp function to produce a robust product 
    // of the entries in x.
    // This allows to prevent underflows or overflows 
    // in most cases.
    //
    // Examples
    // // Basic tests
    // x = [1.e308 1.e308 1.e-308 1.e-308]
    // prod ( x )
    // compdiv_prodfrexp ( x )
    //
    // // Accuracy test
    // x = [
    // -8207398118072301*2^-53
    //  8727187368991500*2^-54
    //  6237403295485065*2^-56
    // -6707097243752269*2^-55
    // -6829010895736168*2^-55
    //  5265283919956334*2^-53
    //  4521970942753754*2^-51
    // -6348554437180427*2^-53
    //  7523931345950583*2^-55
    // -5372571516972004*2^-52
    // ];
    // e = -0.277920000769307861379e-3;
    // r = (compdiv_prodfrexp(x)-e)/e
    //
    // // Random test
    // x = rand(10,1,"normal");
    // r = abs(compdiv_prodfrexp(x)-prod(x))/abs(prod(x))
    //
    // Authors
    // Copyright (C) 2010-2011 - Michael Baudin

  [px,pe]=frexp(x)
  f = prod(px)
  if ( f == 0 ) then
      p = 0
  else
      e = sum(pe)
      [ff,fe]=frexp(f)
      p = ff * 2^(e+fe)
  end
endfunction

