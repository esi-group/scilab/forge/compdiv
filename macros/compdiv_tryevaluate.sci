// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

    function [g,m] = compdiv_tryevaluate ( x , y , express )
    // Try to evaluate all the methods for expression.
    //
    // Calling Sequence
    // [g,m] = compdiv_tryevaluate ( x , y , express )
    //
    // Parameters
    // x : a 1-by-1 matrix of doubles
    // y : a 1-by-1 matrix of doubles
    // express : a 1-by-1 matrix of doubles, express=1,2,3,4,5,6,7,8 the expression.
    // g : a 1-by-1 matrix of doubles, the evaluation of the expression.
    // m : a 1-by-1 matrix of doubles, m=1,2,3, 4, 5, 6, the method which worked.
    //
    // Description
    // This function calls compdiv_evaluate until the 
    // result is non zero, non infinite, non nan.
    // If none of the methods produced such a non-suspect result,
    // the last evaluation is returned.
    //
    // Examples
    // x = 1+2*%i;
    // y = 3+4*%i;
    // z = 11/25 + %i * 2/25;
    // for express = 1:8
    //     [g,m] = compdiv_tryevaluate ( x , y , express )
    // end
    //
    // Authors
    // Copyright (C) 2011 - Michael Baudin

        mmax = 6
        m = 1
        g = compdiv_evaluate ( x , y , express , m )
        while ( g == 0 | isinf(g) | isnan(g) )
            m = m + 1
            if ( m > mmax ) then
                break
            end
            g = compdiv_evaluate ( x , y , express , m )
        end
    endfunction

