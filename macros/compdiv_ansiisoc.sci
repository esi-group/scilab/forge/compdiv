// Copyright (C) 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function z = compdiv_ansiisoc ( x , y )
    // Performs complex division with a port of ANSI/ISO C algorithm.
    //
    // Calling Sequence
    // z = compdiv_ansiisoc(x,y)
    //
    // Parameters
    // x : a matrix of complex doubles
    // y : a matrix of complex doubles
    // z : a matrix of complex doubles, the complex division result z=x/y.
    //
    // Description
    // A Scilab port of the complex division as in C99.
    // Uses a pre-scaling by max(abs(c), abs(d)).
    //
    // Examples
    // expected = 11/25 + %i * 2/25
    // compdiv_ansiisoc ( 1+2*%i , 3+4*%i )
    //
    // Bibliography
    //  ISO/IEC 9899 - Programming languages - C, http://www.open-std.org/jtc1/sc22/wg14/www/docs/n1256.pdf
    //
    // Authors
    // Copyright (C) 2010 - Michael Baudin

    ilogbw = 0
    a = real(x)
    b = imag(x)
    c = real(y)
    d = imag(y)
    [f,logbw] = frexp(max(abs(c), abs(d)))
    if (compdiv_isfinite(logbw)) then
        ilogbw = round(logbw)
        c = compdiv_pow2(c, -ilogbw)
        d = compdiv_pow2(d, -ilogbw)
    end
    den = c * c + d * d
    e = (a * c + b * d) / den
    f = (b * c - a * d) / den
    e = compdiv_pow2(e, -ilogbw)
    f = compdiv_pow2(f, -ilogbw)
    // Recover infinities and zeros that computed as NaN+iNaN */
    // the only cases are nonzero/zero, infinite/finite, and finite/infinite, ... */
    if (isnan(e) & isnan(f)) then
        if ((den == 0.0) & (~isnan(a) | ~isnan(b))) then
            e = compdiv_copysign(%inf, c) * a
            f = compdiv_copysign(%inf, c) * b
        elseif ((isinf(a) | isinf(b)) & compdiv_isfinite(c) & compdiv_isfinite(d)) then
            if (isinf(a)) then
                a = compdiv_copysign(1, a)
            else
                a = compdiv_copysign(0, a)
            end
            if ( isinf(b) ) then
                b = compdiv_copysign(1, b)
            else
                b = compdiv_copysign(0, b)
            end
            e = %inf * ( a * c + b * d )
            f = %inf * ( b * c - a * d )
        elseif (isinf(logbw) & compdiv_isfinite(a) & compdiv_isfinite(b)) then
            if ( isinf(c) ) then
                c = compdiv_copysign(1, c)
            else
                c = compdiv_copysign(0, c)
            end
            if ( isinf(d) ) then
                d = compdiv_copysign(1, d)
            else
                d = compdiv_copysign(0, d)
            end
            e = 0.0 * ( a * c + b * d )
            f = 0.0 * ( b * c - a * d )
        end
    end
    // Avoid using e + %i * f, which may create 
    // %inf+%i*%inf, which actually multiplies 0 and %inf, 
    // generating an unwanted %nan.
    z = complex(e,f)
endfunction

if ( %f ) then

    // 11/25 + %i* 2/25
    r = cdiv_ansiisoC(1+%i*2,3+%i*4) 
    // A case which fails : should be 1e100 - %i * 1e-300
    cdiv_ansiisoC(1e300+%i*1e-300,1e200+%i*1e-200)
end

