// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function compdiv_maclaren ( div )
    // A benchmark suggested by Mac Laren.
    //
    // Calling Sequence
    // compdiv_maclaren ( div )
    //
    // Parameters
    // div : a function, the complex division under test. The function must have the header r=div(x,y).
    //
    // Description
    // This function tests the accuracy of the complex division function <literal>div</literal>
    // based on a sequence of divisions designed by Nick Mac Laren.
    // We consider the division (a+%i*a)/(a+%i*k*a) for a=H/2 and k=0, 0.1, 0.2, ..., 2.0, 
    // where H is the overflow threshold.
    // It is straightforward to factor both the numerator and the 
    // denominator by a, so that the division is equal to (1+%i)/(1+%i*k).
    // This formula is used to compute the exact result, which is printed 
    // with the "E" letter in the result.
    // The computation cannot be carried out for greater values of k, since 
    // this makes the imaginary part of (a+%i*k*a) overflow. 
    // The function <literal>div</literal> is printed with the "D" letter.
    //
    // Examples
    // compdiv_maclaren ( compdiv_scilab )
    //
    // Bibliography
    //  "How Computers Handle Numbers - Some of the Sordid Details", Nick Maclaren, July 2009, http://www-uxsup.csx.cam.ac.uk/courses/Arithmetic/paper_99.pdf
    //
    // Authors
    // Copyright (C) 2011 - Michael Baudin

    a = number_properties("huge")/2;
    for k = (0:20)./10
        x = a+%i*a;
        y = a+%i*k*a;
        c2 = div(x,y);
        exact = compdiv_scilab(1+%i,1+%i*k);
        mprintf("K=%.1f x=(%.3e,%.3e) y=(%.3e,%.3e) E=(%.3f,%.3f) D=(%.3f,%.3f)\n",..
          k,..
          real(x),imag(x),..
          real(y),imag(y),..
          real(exact),imag(exact),..
          real(c2),imag(c2));
    end
endfunction

