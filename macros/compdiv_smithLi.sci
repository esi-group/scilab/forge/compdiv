// Copyright (C) 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function z = compdiv_smithLi ( x,y )
    // Performs complex division with a Smith method, improved by Li et al.
    //
    // Calling Sequence
    // z = compdiv_smithLi (x,y)
    //
    // Parameters
    // x : a matrix of complex doubles
    // y : a matrix of complex doubles
    // z : a matrix of complex doubles, the complex division result z=x/y.
    //
    // Description
    // This algorithms uses Smith's method and performs additionnal up and down scaling.
    //
    // Examples
    // expected = 11/25 + %i * 2/25
    // compdiv_smithLi ( 1+2*%i , 3+4*%i )
    //
    // Bibliography
    //  "Design, implementation and testing of extended and mixed precision BLAS", Xiaoye S. Li, James W. Demmel, David H. Bailey, Greg Henry, Yozo Hida, Jimmy Iskandar, William Kahan, Suh Y. Kang, Anil Kapur, Michael C. Martin, Brandon J. Thompson, Teresa Tung, and Daniel J. Yoo. 2002. ACM Trans. Math. Softw. 28, 2 (June 2002), 152-205. 
    //
    // Authors
    // Copyright (C) 2010 - Michael Baudin

    a = real(x)
    b = imag(x)
    c = real(y)
    d = imag(y)
    AB = max(abs([a b]))
    CD = max(abs([c d]))
    // The choice of Li et al. for B:
    B = 2
    S = 1
    OV = number_properties("huge")
    UN = number_properties("tiny")
    // Scaling
    if ( AB > OV/16 ) then
      // Scale down a, b
      x = x/16
      S = S*16
    end
    if ( CD > OV/16 ) then
      // Scale down c, d
      y = y/16
      S = S/16
    end
    if ( AB < UN*B/%eps ) then
      // Scale up a, b
      Be = B/%eps^2
      x = x * Be
      S = S/Be
    end
    if ( CD < UN*B/%eps ) then
      // Scale up c, d
      Be = B/%eps^2
      y = y * Be
      // The paper has a mistake there
      S = S*Be
    end
    // Now a, b, c, d in [UN*B/%eps,OV/16]
    z = compdiv_smith ( x , y )
    // Scale back
    z = z * S
endfunction

