// Copyright (C) 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt



function X = compdiv_pow2 ( varargin )
    // Base 2 power and scale floating-point numbers.
    //
    // Calling Sequence
    // X = compdiv_pow2 ( Y )
    // X = compdiv_pow2 ( F , E )
    //
    // Parameters
    // X : a matrix of doubles
    // Y : a matrix of doubles
    // F : a matrix of doubles
    // E : a matrix of doubles
    //
    // Description
    // X = compdiv_pow2(Y) returns 2^Y
    //
    // X = compdiv_pow2(F,E) returns F.* 2 .^ E.
    //
    // compdiv_pow2(1-%eps/2,1024) produces realmax (and not %inf)
    //
    // This function corresponds to the ANSI C function ldexp() and the IEEE floating-point standard function scalbn().
    //
    // Examples
    //     F = [1/2 %pi/4 -3/4 1/2 1-%eps/2 1/2];
    //     E = [1 2 2 -51 1024 -1021];
    //     X = compdiv_pow2 ( F , E )
    //     realmax = number_properties("huge")
    //     realmin = number_properties("tiny")
    //     expected = [1 %pi -3 %eps  realmax realmin]
    //     X == expected // T T T T F T
    //
    // Bibliography
    // http://www.mathworks.fr/help/techdoc/ref/pow2.html
    //
    // Authors
    // Copyright (C) 2010 - Michael Baudin

    [lhs,rhs]=argn()
    if ( and( rhs<>[1 2] ) ) then
        errmsg = sprintf(gettext("%s: Unexpected number of arguments : %d provided while %d to %d are expected."),..
        "compdiv_pow2",rhs,1,2)
        error(errmsg)
    end
    if ( rhs==1 ) then
        Y = varargin(1)
        X = 2.^Y
    else
        F = varargin(1)
        E = varargin(2)
        // Avoid overflow in limit cases.
        [m,n]=frexp(F)
        X = (m*2).* 2 .^ (E+n-1)
    end
endfunction

