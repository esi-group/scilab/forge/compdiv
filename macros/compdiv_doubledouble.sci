// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function z = compdiv_doubledouble ( varargin )
    // Creates a double-double.
    //
    // Calling Sequence
    // z = compdiv_doubledouble()
    // z = compdiv_doubledouble(high)
    // z = compdiv_doubledouble(high,low)
    //
    // Parameters
    // high : a 1-by-1 matrix of real doubles (no complex), the high-order part (default high=0)
    // low : a 1-by-1 matrix of real doubles (no complex), the low-order part (default low=0)
    // z : a double-double, z=z.high+z.low
    // z.high : the high-order part of z
    // z.low : the low-order part of z
    //
    // Description
    // Creates a double-double from a double
    //
    // Examples
    // z1 = compdiv_doubledouble ( 2 )
    // z2 = compdiv_doubledouble ( 3 )
    // z1+z2
    // z1*z2
    // z1/z2
    //
    // Bibliography
    //  "Design, Implementation and Testing of Extended and Mixed Precision BLAS", Li et al, 2000
    //
    // Authors
    // Copyright (C) 2011 - Michael Baudin

    function argin = argindefault ( rhs , vararglist , ivar , default )
        // Returns the value of the input argument #ivar.
        // If this argument was not provided, or was equal to the 
        // empty matrix, returns the default value.
        if ( rhs < ivar ) then
            argin = default
        else
            if ( vararglist(ivar) <> [] ) then
                argin = vararglist(ivar)
            else
                argin = default
            end
        end
    endfunction

    [lhs,rhs]=argn()
    apifun_checkrhs ( "compdiv_doubledouble" , rhs , 0:2 )
    apifun_checklhs ( "compdiv_doubledouble" , lhs , 0:1 )
    //
    high = argindefault ( rhs , varargin , 1 , 0 )
    low = argindefault ( rhs , varargin , 2 , 0 )

    z = tlist([
    "DBLDBL"
    "high"
    "low"
    ])
    z.high = high
    z.low = low
endfunction

