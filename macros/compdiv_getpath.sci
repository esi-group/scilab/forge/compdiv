// Copyright (C) 2010 - 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function path = compdiv_getpath (  )
    // Returns the path to the current module.
    // 
    // Calling Sequence
    //   path = compdiv_getpath (  )
    //
    // Parameters
    //   path : a 1-by-1 matrix of strings, the path to the current module.
    //
    // Examples
    //   path = compdiv_getpath (  )
    //
    // Authors
    //   Copyright (C) 2010 - DIGITEO - Michael Baudin

    path = get_function_path("compdiv_getpath")
    path = fullpath(fullfile(fileparts(path),".."))
endfunction

