// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function z = compdiv_smithSZ ( x,y )
    // Simulates Smith's algorithm on a store-zero system.
    //
    // Calling Sequence
    // z = compdiv_smithSZ ( x,y )
    //
    // Parameters
    // x : a matrix of complex doubles
    // y : a matrix of complex doubles
    // z : a matrix of complex doubles, the complex division result z=x/y.
    //
    // Description
    // Uses Smith's algorithm to perform the complex division.
    // Applies compdiv_storezero function on all intermediate 
    // computations.
    // For example, (a + b * r) / den is evaluated as SZ(SZ(a + SZ(b * r)) / den), 
    // where SZ=compdiv_storezero, the store-zero function is applied to the 
    // product b*r, the sum and the division.
    //
    // Examples
    // expected = 11/25 + %i * 2/25
    // compdiv_smithSZ ( 1+2*%i , 3+4*%i )
    //
    // x = rand(3,5)+%i*rand(3,5)
    // y = rand(3,5)+%i*rand(3,5)
    // z = compdiv_smithSZ ( x , y )
    // abs(z.*y - x)./abs(x)
    //
    // Authors
    // Copyright (C) 2011 - Michael Baudin

    for i = 1 : size(x,"r")
    for j = 1 : size(x,"c")
    z(i,j) = internal_smithSZ ( x(i,j) , y(i,j) )
    end
    end
endfunction

function z = internal_smithSZ ( x , y )
    SZ = compdiv_storezero
    a = real(x)
    b = imag(x)
    c = real(y)
    d = imag(y)
    if ( abs(d) <= abs(c) ) then
        r = SZ(d/c)        
        den = SZ(c + SZ(d * r))
        e = SZ(SZ(a + SZ(b * r)) / den)
        f = SZ(SZ(b - SZ(a * r)) / den)
    else
        r = SZ(c/d)
        den = SZ(SZ(c * r) + d)
        e = SZ(SZ(SZ(a * r) + b) / den)
        f = SZ(SZ(SZ(b * r) - a) / den)
    end
    // Avoid using x + %i * y, which may create
    // %inf+%i*%inf, which actually multiplies 0 and %inf,
    // generating an unwanted %nan.
    z = complex(e,f)
endfunction


