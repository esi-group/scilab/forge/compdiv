// Copyright (C) 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function tf = compdiv_isfinite ( A )
    // Array elements that are finite.
    //
    // Calling Sequence
    // tf = compdiv_isfinite ( A )
    //
    // Parameters
    // A : a matrix of doubles
    // tf : a matrix of booleans
    //
    // Description
    // tf(i) is true if A(i) is finite, and tf(i) is false if A(i) is infinite or nan.
    //
    // Examples
    //   ieee(2)
    //   a = [-2  -1  0  1  2];
    //   compdiv_isfinite (1 ./ a ) // T T F T T
    //
    // Bibliography
    //  http://www.mathworks.fr/help/techdoc/ref/isfinite.html
    //
    // Authors
    // Copyright (C) 2010 - Michael Baudin

    tf = (abs(A)<%inf)
endfunction

