// Copyright (C) 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function z = compdiv_smith ( x,y )
    //   Returns the complex division z = x / y  by Smith's method.
    //
    // Calling Sequence
    // z = compdiv_smith ( x,y )
    //
    // Parameters
    // x : a matrix of complex doubles
    // y : a matrix of complex doubles
    // z : a matrix of complex doubles, the complex division result z=x/y.
    //
    // Description
    // Uses Smith's algorithm to perform the complex division.
    // The scaling is based on the comparison of d and c.
    //
    // The cost is 3 floating point divisions, 3 floating point multiplications,
    // 3 floating point additions.
    // The total cost is 9 floating point operations.
    //
    // Examples
    // expected = 11/25 + %i * 2/25
    // compdiv_smith ( 1+2*%i , 3+4*%i )
    //
    // x = rand(3,5)+%i*rand(3,5)
    // y = rand(3,5)+%i*rand(3,5)
    // z = compdiv_smith ( x , y )
    // abs(z.*y - x)./abs(x)
    //
    // Authors
    // Copyright (C) 2010 - Michael Baudin
    
    for i = 1 : size(x,"r")
    for j = 1 : size(x,"c")
    z(i,j) = compdiv_smithS ( x(i,j) , y(i,j) )
    end
    end
endfunction

function z = compdiv_smithS ( x , y )
    a = real(x)
    b = imag(x)
    c = real(y)
    d = imag(y)
    if ( abs(d) <= abs(c) ) then
        r = d/c
        den = c + d * r
        e = (a + b * r) / den
        f = (b - a * r) / den
    else
        r = c/d
        den = c * r + d
        e = (a * r + b) / den
        f = (b * r - a) / den
    end
    // Avoid using x + %i * y, which may create
    // %inf+%i*%inf, which actually multiplies 0 and %inf,
    // generating an unwanted %nan.
    z = complex(e,f)
endfunction

