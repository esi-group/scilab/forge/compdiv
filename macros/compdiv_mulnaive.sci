// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function r = compdiv_mulnaive (x,y)
    // Performs complex multiplication with a naive algorithm.
    //
    // Calling Sequence
    // r = compdiv_mulnaive (x,y)
    //
    // Parameters
    // x : a matrix of complex doubles
    // y : a matrix of complex doubles
    // r : a matrix of complex doubles, the complex multiplication result r=x*y.
    //
    // Description
    // Performs complex multiplication with a naive algorithm.
    //
    // Examples
    // expected = -5 + %i * 10
    // compdiv_mulnaive ( 1+2*%i , 3+4*%i )
    //
    // x = rand(3,5)+%i*rand(3,5)
    // y = rand(3,5)+%i*rand(3,5)
    // r = compdiv_mulnaive ( x , y )
    // abs(r - x.*y)./abs(x)
    //
    // Bibliography
    //  "Scilab is not naive", Baudin, 2011
    //
    // Authors
    // Copyright (C) 2011 - Michael Baudin

n = size(x,"r")
m = size(x,"c")
for i = 1 : n
for j = 1 : m
r(i,j) = mulnaive_internal(x(i,j),y(i,j))
end
end

endfunction
function r = mulnaive_internal(x,y)
  a = real(x)
  b = imag(x)
  c = real(y)
  d = imag(y)
  r = complex(a.*c-b.*d,a.*d+b.*c)
endfunction

