// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [digits,x,y,z,q] = compdiv_testdataset ( varargin )
    // Test a complex division on a dataset
    //
    // Calling Sequence
    // digits = compdiv_testdataset ( dataset , div )
    // digits = compdiv_testdataset ( dataset , div , rows )
    // digits = compdiv_testdataset ( dataset , div , rows , verbose )
    // [digits,x] = compdiv_testdataset ( ... )
    // [digits,x,y] = compdiv_testdataset ( ... )
    // [digits,x,y,z] = compdiv_testdataset ( ... )
    // [digits,x,y,z,q] = compdiv_testdataset ( ... )
    //
    // Parameters
    // dataset : a 1-by-1 matrix of strings, the dataset. 
    // div : a function, the complex division under test. The function must have the header r=div(x,y).
    // rows : a m-by-1 matrix of doubles, the rows to read in the dataset (default rows=[]). If rows=[], all rows are read. If not, the rows to read in the dataset.
    // digits : a m-by-1 matrix of doubles, the number of significant digits, for each of the m experiments.
    // verbose : a 1-by-1 matrix of booleans, the verbose mode (default verbose=%f). Set to verbose=%t to enable verbose mode.
    // x : a m-by-1 matrix of doubles, the numerator.
    // y : a m-by-1 matrix of doubles, the denominator.
    // z : a m-by-1 matrix of doubles, the expected x/y.
    // q : a m-by-1 matrix of doubles, the computed x/y.
    // 
    // Description
    // The dataset is expected to be in .csv format, and contain 6 columns, 
    // a, b, c, d, e, and f, where x=complex(a,b) is the numerator, 
    // y = complex(c,d) is the denominator, and 
    // z = complex(e,f) is the expected division x/y.
    //
    // The available tests are sorted by increasing order of difficulty.
    // <itemizedlist>
    //     <listitem><para>
    //         Tests #1 to #88 are easy.
    //     </para></listitem>
    //     <listitem><para>
    //         Tests #89 to #100 are divisions by zero.
    //     </para></listitem>
    //     <listitem><para>
    //         Tests #101 to #132 are so that one of the arguments 
    //         a,b,c or d is close to overflow, the others are unity.
    //         The naive formula fails in this case.
    //     </para></listitem>
    //     <listitem><para>
    //         Tests #133 to #154 are so that Smith's (1962) algorithm
    //         fails, with a zero imaginary part created by underflow.
    //     </para></listitem>
    //     <listitem><para>
    //         Tests #155 to #195 are so that Smith's (1962) algorithm
    //         fails, with an infinite real part created by overflow 
    //         (a=b is large, c=d varies).
    //     </para></listitem>
    //     <listitem><para>
    //         Tests #196 to #236 are so that Smith's (1962) algorithm
    //         fails, with a zero result created by overflow 
    //         (a=b varies, c=d is large).
    //     </para></listitem>
    //     <listitem><para>
    //         Tests #237 to the #281 are empirically failure tests cases.
    //     </para></listitem>
    //     <listitem><para>
    //         Test #282 is extremely difficult.
    //     </para></listitem>
    // </itemizedlist>
    //
    // Examples
    // ieee(2);
    // path = compdiv_getpath();
    // dataset = fullfile(path,"tests","unit_tests","cdiv.dataset.csv");
    // [digits,x,y,z,q] = compdiv_testdataset ( dataset , compdiv_smith );
    //
    // // Test only divisions by zero
    // compdiv_testdataset ( dataset , compdiv_smith , 89:100)
    //
    // // Enable verbose mode on the easy tests
    // compdiv_testdataset ( dataset , compdiv_smith , 1:88,%t);
    //
    // Authors
    // Copyright (C) 2011 - Michael Baudin

    function s = myformatd(d)
        if ( isnan(d) | isinf(d) ) then
            s = msprintf("%10s",string(d))
        else
            s = msprintf("%+.3e",d)
        end
    endfunction
    function s = myformatcomp(d)
        s = msprintf("(%12s,%12s)",myformatd(real(d)),myformatd(imag(d)))
    endfunction
    function argin = argindefault ( rhs , vararglist , ivar , default )
        // Returns the value of the input argument #ivar.
        // If this argument was not provided, or was equal to the 
        // empty matrix, returns the default value.
        if ( rhs < ivar ) then
            argin = default
        else
            if ( vararglist(ivar) <> [] ) then
                argin = vararglist(ivar)
            else
                argin = default
            end
        end
    endfunction
    //
    [lhs, rhs] = argn()
    apifun_checkrhs ( "compdiv_testdataset" , rhs , 2:4 )
    apifun_checklhs ( "compdiv_testdataset" , lhs , 0:5 )
    //
    // Get arguments
    dataset = varargin ( 1 )
    div = varargin ( 2 )
    rows = argindefault ( rhs , varargin , 3 , [] )
    verbose  = argindefault ( rhs , varargin , 4 , %f )
    //
    // Check type
    apifun_checktype ( "compdiv_testdataset" , dataset , "dataset" , 1 , "string" )
    apifun_checktype ( "compdiv_testdataset" , div , "div" , 2 , ["function" "fptr"] )
    apifun_checktype ( "compdiv_testdataset" , rows , "rows" , 3 , "constant" )
    apifun_checktype ( "compdiv_testdataset" , verbose , "verbose" , 4 , "boolean" )
    //
    // Check size
    apifun_checkscalar ( "compdiv_testdataset" , dataset , "dataset" , 1 )
    if ( rows <> [] ) then
        apifun_checkvector ( "compdiv_testdataset" , rows , "rows" , 3 , size(rows,"*") )
    end
    apifun_checkscalar ( "compdiv_testdataset" , verbose , "verbose" , 4 )
    //
    // Check content
    apifun_checkgreq ( "compdiv_testdataset" , rows , "rows" , 3 , 1 )
    //
    table = compdiv_datasetread ( dataset , "#" , "," , verbose );
    ntests = size(table,"r");
    atable = evstr(table(:,1));
    btable = evstr(table(:,2));
    ctable = evstr(table(:,3));
    dtable = evstr(table(:,4));
    etable = evstr(table(:,5));
    ftable = evstr(table(:,6));
    x = complex(atable,btable);
    y = complex(ctable,dtable);
    z = complex(etable,ftable);
    basis = 2
    //
    // Set kindices as the indices of rows to read.
    if ( rows == [] ) then
        kindices = 1 : ntests
    else
        apifun_checkloweq ( "compdiv_testdataset" , rows , "rows" , 3 , ntests )
        // Force to a row vector.
        kindices = rows(:)'
        // Extract the data that is to be used.
        x=x(kindices)
        y=y(kindices)
        z=z(kindices)
    end
    //
    nreduced = size(kindices,"*")
    //
    for j = 1 : nreduced
        k = kindices(j)
        //
        q(j) = div ( x(j) , y(j) );
        digits(j) = compdiv_computedigits(z(j),q(j),basis);
        if ( verbose ) then
            mprintf("Test #%3d/%d, x=%s, y=%s, z=%s, div=%s, D=%.1f\n",  ..
            k,ntests,..
            myformatcomp(x(j)) , myformatcomp(y(j)) , myformatcomp(z(j)) , ..
            myformatcomp(q(j)) , digits(j) );
        end
    end
    if ( verbose ) then
        mprintf("--\n");
        mprintf("Summary:\n");
        nbsucc = size(find(digits==53),"*")
        nbfail = size(find(digits==0),"*")
        nbinter = size(find(0<digits&digits<53),"*")
        mprintf("Number of full success:%d\n",nbsucc);
        mprintf("Number of full failure:%d\n",nbfail);
        mprintf("Number of full success:%d\n",nbinter);
        mprintf("Mean. number of digits:%d\n",mean(digits));
        mprintf("Max. number of digits:%d\n",max(digits));
        mprintf("Min. number of digits:%d\n",min(digits));
    end
endfunction

