// Copyright (C) 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function r = compdiv_polar ( x , y )
    // A complex division based on polar form.
    //
    // Calling Sequence
    // r = compdiv_polar ( x , y )
    //
    // Parameters
    // x : a matrix of complex doubles
    // y : a matrix of complex doubles
    // r : a matrix of complex doubles, the complex division result r=x/y.
    //
    // Description
    // Transforms into polar form x=r*exp(i*u) and y = x=t*exp(i*w), compute division 
    // z=(r/t)*exp(i*(u-w)) and convert back to cartesian.
    // May be inaccurate.
    //
    // Examples
    // expected = 11/25 + %i * 2/25
    // compdiv_polar ( 1+2*%i , 3+4*%i )
    //
    // // Can be very inaccurate:
    // x=-1-%i
    // y=-1+%i
    // compdiv_polar ( x , y ) // - 1.837D-16 + i but exact = %i
    //
    // Authors
    // Copyright (C) 2010 - Michael Baudin

    // Put x in to polar form x=xr*exp(xa*%i)
    xr = abs(x)
    xa = atan(imag(x),real(x))
    // Put y in to polar form y=yr*exp(ya*%i)
    yr = abs(y)
    ya = atan(imag(y),real(y))
    // Divide : (x/y)
    r = (xr/yr)*exp((xa-ya)*%i)
endfunction

