// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function digits = compdiv_extreme ( div )
    // Test extreme complex divisions.
    //
    // Calling Sequence
    // digits = compdiv_extreme ( div )
    //
    // Parameters
    // div : a function, the complex division under test. The function must have the header r=div(x,y).
    // digits : a 54-by-4 matrix of doubles, the number of binary digits for the tests. digits(i,j) is the number of significant digits in the test with a=omega
    //
    // Description
    // This function tests the accuracy of the complex division function <literal>div</literal>
    // based on a sequence of divisions.
    // These extreme divisions are inspired by the benchmark created by Nick Mac Laren.
    // 
    // For i=0, 1, 2, ..., 53, we set k = 2^i and set a=Omega/k where 
	// Omega is the overflow threshold.
    // In all cases, we have k*a=Omega.
    // We test 4 sets of divisions. Set j=1 corresponds to (k*a+%i*a)/(a+%i*a), 
    // set j=2 to (a+%i*k*a)/(a+%i*a), set j=3 to (a+%i*a)/(k*a+%i*a) and 
    // set j=4 to (a+%i*a)/(a+%i*k*a).
    //
    // It is straightforward to factor both the numerator and the 
    // denominator by a, so that the division, in set #4 for example, 
    // is equal to (1+%i)/(1+%i*k).
    // This formula is used to compute the exact result, which is printed 
    // with the "E" letter in the result.
    //
    // The function <literal>div</literal> is printed with the "D" letter.
    //
    // Examples
    // compdiv_extreme ( compdiv_scilab )
    //
    // Bibliography
    //  "How Computers Handle Numbers - Some of the Sordid Details", Nick Maclaren, July 2009, http://www-uxsup.csx.cam.ac.uk/courses/Arithmetic/paper_99.pdf
    //
    // Authors
    // Copyright (C) 2011 - Michael Baudin

    function digits = extreme_print(i,k,x,y,exact)
        c2 = div(x,y);
        digits = compdiv_computedigits(c2,exact,2)
        mprintf("k=2^%.1f x=(%.2e,%.2e) y=(%.2e,%.2e) E=(%+.3e,%+.3e) D=(%+.3e,%+.3e) bits=%.1f\n",..
        i,..
        real(x),imag(x),..
        real(y),imag(y),..
        real(exact),imag(exact),..
        real(c2),imag(c2),..
        digits);
    endfunction
    function digits = print_foursets ( a , i, k )
        x = k*a+%i*a;
        y = a+%i*a;
        exact = compdiv_scilab(k+%i,1+%i);
        digits(1) = extreme_print(i,k,x,y,exact)
        //
        x = a+%i*k*a;
        y = a+%i*a;
        exact = compdiv_scilab(1+%i*k,1+%i);
        digits(2) = extreme_print(i,k,x,y,exact)
        //
        x = a+%i*a;
        y = k*a+%i*a;
        exact = compdiv_scilab(1+%i,k+%i);
        digits(3) = extreme_print(i,k,x,y,exact)
        //
        x = a+%i*a;
        y = a+%i*k*a;
        exact = compdiv_scilab(1+%i,1+%i*k);
        digits(4) = extreme_print(i,k,x,y,exact)
    endfunction
    //
	omega = number_properties("huge")
    for i=0:54
        k = 2^i
        mprintf("Case #%d: a=omega/(2^%i)\n", i,i);
        a = omega/k;
        digits(i+1,1:4) = print_foursets ( a , i , k )'
    end
endfunction

