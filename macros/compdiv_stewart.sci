// Copyright (C) 2010 - 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function z = compdiv_stewart ( x,y )
    // Performs complex division with Stewart's algorithm.
    //
    // Calling Sequence
    // z = compdiv_stewart (x,y)
    //
    // Parameters
    // x : a matrix of complex doubles
    // y : a matrix of complex doubles
    // z : a matrix of complex doubles, the complex division result z=x/y.
    //
    // Description
    // This algorithms uses Smith's method but performs robust multiplications a*b*c,
    // avoiding unnecessary overflows.
    //
    // Examples
    // expected = 11/25 + %i * 2/25
    // compdiv_stewart ( 1+2*%i , 3+4*%i )
    //
    // x = rand(3,5)+%i*rand(3,5)
    // y = rand(3,5)+%i*rand(3,5)
    // r = compdiv_stewart ( x , y )
    // abs(r.*y - x)./abs(x)
    //
    // Bibliography
    //  "A Note on Complex Division", G. W. Stewart, University of Maryland, ACM Transactions on Mathematical Software, Vol. 11, No 3, September 1985, Pages 238-241
    //
    // Corrigendum: "A Note on Complex Division",G. W. Stewart, TOMS, Volume 12, Number 3, Pages = 285--285, September, 1986
    //
    // Authors
    // Copyright (C) 2010-2011 - Michael Baudin

    for i = 1 : size(x,"r")
    for j = 1 : size(x,"c")
    z(i,j) = compdiv_stewartS ( x(i,j) , y(i,j) )
    end
    end
endfunction
function z = compdiv_stewartS ( x,y )
// Set z := x/y
    a = real(x)
    b = imag(x)
    c = real(y)
    d = imag(y)

    flip = %f
    if ( abs(d) >= abs(c) ) then
      [d,c]=switch(c,d)
      [a,b]=switch(b,a)
      flip=%t
    end
    s=1/c
    t=1/(c+d*(d*s))
    if ( abs(d) >= abs(s) ) then
      [d,s]=switch(s,d)
    end
    if ( abs(b) >= abs(s) ) then
      e=t*(a+s*(b*d))
    elseif ( abs(b) >= abs(d) ) then
      e=t*(a+b*(s*d))
    else
      e=t*(a+d*(s*b))
    end
    if ( abs(a) >= abs(s) ) then
      f=t*(b-s*(a*d))
    elseif ( abs(a) >= abs(d) ) then
      f=t*(b-a*(s*d))
    else
      f=t*(b-d*(s*a))
    end
    if ( flip ) then
      f=-f
    end
    // Avoid using x + %i * y, which may create
    // %inf+%i*%inf, which actually multiplies 0 and %inf,
    // generating an unwanted %nan.
    z = complex(e,f)
endfunction

function [c,d]=switch(a,b)
  c=a
  d=b
endfunction

