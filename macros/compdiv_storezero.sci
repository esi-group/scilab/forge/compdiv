// Copyright (C) 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function y = compdiv_storezero(x)
    // Returns the floating point number fl(x) on a store-zero system.
    //
    // Calling Sequence
    // z = compdiv_storezero(x,y)
    //
    // Parameters
    // x : a 1-by-1 matrix of real doubles
    // y : a 1-by-1 matrix of real doubles
    //
    // Description
    // Let us denote by UN the smallest positive nonzero 
    // normalized floating point number.
    // If x has a magnitude greater than UN, returns x.
    // If not, returns zero.
    //
    // This allows to simulate store-zero on a floating point system with gradual 
    // underflow.
    //
    // Examples
    // UN = number_properties("tiny")
    // // Returns x:
    // compdiv_storezero ( UN ) 
    // compdiv_storezero ( -UN ) 
    // compdiv_storezero ( 2*UN )
    // compdiv_storezero ( -2*UN )
    // // Store them as zero:
    // compdiv_storezero ( 0.5*UN )
    // compdiv_storezero ( -0.5*UN )
    //
    // Bibliography
    //  ISO/IEC 9899 - Programming languages - C, http://www.open-std.org/jtc1/sc22/wg14/www/docs/n1256.pdf
    //
    // Authors
    // Copyright (C) 2011 - Michael Baudin

  UN = number_properties("tiny")
  if (abs(x)<UN) then
    y = 0
  else
    y = x
  end
endfunction

