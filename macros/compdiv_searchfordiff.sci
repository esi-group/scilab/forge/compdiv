// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function [j , p, s, k] = compdiv_searchfordiff ( varargin )
    // Search for differences between 2 algorithms.
    //
    // Calling Sequence
    // compdiv_searchfordiff ( div1 , div2 )
    // compdiv_searchfordiff ( div1 , div2 , nmatrix )
    // compdiv_searchfordiff ( div1 , div2 , nmatrix , kmax )
    // compdiv_searchfordiff ( div1 , div2 , nmatrix , kmax , rtol )
    // compdiv_searchfordiff ( div1 , div2 , nmatrix , kmax , rtol , atol )
    // compdiv_searchfordiff ( div1 , div2 , nmatrix , kmax , rtol , atol , verbose )
    // compdiv_searchfordiff ( div1 , div2 , nmatrix , kmax , rtol , atol , verbose , block )
    // compdiv_searchfordiff ( div1 , div2 , nmatrix , kmax , rtol , atol , verbose , block , minbits )
    // j = compdiv_searchfordiff ( ... )
    // [j , p] = compdiv_searchfordiff ( ... )
    // [j , p, s] = compdiv_searchfordiff ( ... )
    // [j , p, s, k] = compdiv_searchfordiff ( ... )
    //
    // Parameters
    // div1 : a function r=div1(x,y) which returns r=x/y
    // div2 : a function r=div2(x,y) which returns r=x/y (The reference algorithm)
    // nmatrix : a 4-by-m matrix of doubles (default nmatrix=[-1074:1023;-1074:1023;-1074:1023;-1074:1023]), the indices to use for na,nb,nc,nd. nmatrix(1,1:2) is the min/max for na, nmatrix(2,1:2) is the min/max for nb, nmatrix(3,1:2) is the min/max for nc, nmatrix(4,1:2) is the min/max for nd.
    // kmax : a 1-by-1 matrix of floating point integers (default kmax=100), the number of Monte-Carlo experiments.
    // rtol : a 1-by-1 matrix of doubles, the relative tolerance (default rtol=1.e-2).
    // atol : a 1-by-1 matrix of doubles, the absolute tolerance (default atol=0)
    // verbose : a 1-by-1 matrix of doubles, or a function, or a list. If verbose is a 1-by-1 matrix of doubles, set to 0, 1, 2 or 3 (default verbose=0). Set to verbose=0 to print no messages, set to verbose=1 to print one message every 10000 iterations, set to verbose=2 to print an operation each time the minimum number significant digits is in the top worst, set to verbose=3 to print all operations which are different. If verbose is a function or a list, it is considered as an ouput function. See below for details.
    // block : a 1-by-1 matrix of booleans (default block=%f), set to true to enable block algorithm.
    // minbits : a 1-by-1 matrix of doubles, integer value, the number of require significant binary digits (default minbits = 51)
    // j : a 1-by-1 matrix of doubles, the number of times that div1 produced a different q than div2.
    // p : a 1-by-1 matrix of doubles, the probability that div1 produces a different result than div2. We have p=j/k, where k is the number of iterations performed.
    // s : a 1-by-1 matrix of doubles, the standard deviation of this Bernouilli process. We have s^2=p*(1-p)/k, where k is the number of iterations performed.
    // k : a 1-by-1 matrix of doubles, the number of iterations. We always have k <= kmax. We have k<kmax in the case where the relative or absolute tolerances interrupted the algorithm.
    //
    // Description
    // Search for [a,b,c,d]=[2^na,2^nb,2^nc,2^nd]
    // such that an operation function div1 gives a different 
    // result than div2.
    //
    // This function modifies the state of the grand function.
    //
    // The <literal>verbose</literal> variable can be a boolean or a function. 
    // This function allows to customize the printed messages.
    // The function <literal>verbose</literal> should have header
    //   <programlisting>
    //     stop = verbose ( k , j , x , y , q1 , q2 , p, s, status ) 
    //   </programlisting>
    // where k is the simulation index, j is the total number of cases where the results 
    // were different, x is the numerator of the complex operation, y is the 
    // denominator of the complex operation, q1 is the result of the complex operation 
    // function div1, q2 is the result of the complex operation 
    // function div2, p is the current failure probability, s is the current standard 
    // deviation and status is the current verbose status.
    // The status is a 1-by-1 matrix of strings with value status="verb" or status="diff".
    // If status=="verb", this means that the function is called back to print the current 
    // status.
    // If status=="diff", this means that q1 is different from q2, i.e. a 
    // difficult complex operation has been found.
    // Here, stop is a boolean, which is %t if the algorithm must stop.
    //
    // It might happen that the output function requires additionnal arguments to be evaluated.
    // In this case, we can use the following feature.
    // The <literal>verbose</literal> variable can be the list (f,a1,a2,...), where 
    // f is a function with header
    // The output function outfun can also be a list, with header 
    //   <programlisting>
    //     stop = f ( k , j , x , y , q1 , q2 , p, s, status, a1 , a2 , ... ) 
    //   </programlisting>
    // and the input arguments a1, a2, ...
    // will be automatically be appended at the end of the calling sequence.
    //
    // Examples
    // ieee(2);
    // stacksize("max");
    // // With default parameters
    // [j , p, s, k] = compdiv_searchfordiff ( compdiv_naive , compdiv_scilab );
    // // With verbose mode
    // [j , p, s, k] = compdiv_searchfordiff ( compdiv_naive , compdiv_scilab , ..
    //    [],[],[],[],1);
    // //
    // // With customized options
    // nmatrix = [
    // -1074:1023
    // -1074:1023
    // -1074:1023
    // -1074:1023
    // ];
    // kmax = 100000;
    // rtol = 1.e-1;
    // atol = 0;
    // verbose = 1;
    // compdiv_searchfordiff ( compdiv_naive , compdiv_scilab , nmatrix , kmax , ..
    //    rtol , atol , verbose );
    //
    // // See the performance of an algorithm.
    // compdiv_searchfordiff ( compdiv_improved , compdiv_scilab , nmatrix , 10000 , ..
    //    1.e-1 , atol , 2 , 100 );
    //
    // // Display the worst cases
    // verbose = 2;
    // compdiv_searchfordiff ( compdiv_naive , compdiv_scilab , [],[],[],[], verbose );
    //
    // // Display all
    // verbose = 3;
    // compdiv_searchfordiff ( compdiv_naive , compdiv_scilab , [],[],[],[], verbose );
    //
    // // Try much harder cases.
    // nmatrix = [-1074:-900 900:1023; -1074:-900 900:1023; -1074:-900 900:1023; -1074:-900 900:1023 ];
    // verbose = 2;
    // compdiv_searchfordiff ( compdiv_naive , compdiv_scilab , nmatrix, [],[],[], verbose );
    // compdiv_searchfordiff ( compdiv_smith10 , compdiv_scilab , nmatrix , [],[],[] , verbose );
    //
    // // To explore the larger exponents:
    // nmatrix = [ 900:1023; 900:1023; 900:1023; 900:1023 ];
    // // To explore the smaller exponents:
    // nmatrix = [ -1074:-900; -1074:-900; -1074:-900; -1074:-900 ];
    //
    // // Customize the verbose function
    // function tf = myverbose ( k , j , x , y , q1 , q2 , p, s, state )
    //     tf = %f
    //     if (state=="verb"& modulo(k,100)==0) then
    //         pmin = p-1.96*s
    //         pmax = p+1.96*s
    //         mprintf("k=%d, j=%d, p=%.3e, 95%%=[%.2e,%.2e]\n",k,j,p,pmin,pmax);
    //     elseif ( state=="diff" ) then
    //         na = int(log2(real(x)))
    //         nb = int(log2(imag(x)))
    //         nc = int(log2(real(y)))
    //         nd = int(log2(imag(y)))
    //         mprintf("Difference identified\n");
    //         mprintf("(2^%d+%%i*2^%d)/(2^%d+%%i*2^%d)\n",na,nb,nc,nd);
    //         mprintf("  Div1=%s+%%i*%s\n",string(real(q1)),string(imag(q1)))
    //         mprintf("  Div2=%s+%%i*%s\n",string(real(q2)),string(imag(q2)))
    //     end
    // endfunction
    // compdiv_searchfordiff ( compdiv_naive , compdiv_scilab,[],[],[],[],myverbose );
    //
    // // A verbose function with one line by message
    // function tf = myverbosebrief ( k , j , x , y , q1 , q2 , p, s, state )
    //     function s = myformatd(d)
    //         if ( isnan(d) | isinf(d) ) then
    //             s = msprintf("%10s",string(d))
    //         else
    //             s = msprintf("%+.3e",d)
    //         end
    //     endfunction
    //     function s = myformatcomp(d)
    //         s = msprintf("(%12s,%12s)",myformatd(real(d)),myformatd(imag(d)))
    //     endfunction
    //
    //     tf = %f
    //     if (state=="verb"& modulo(k,100)==0) then
    //         pmin = p-1.96*s
    //         pmax = p+1.96*s
    //         mprintf("k=%d, j=%d, p=%.3e, 95%%=[%.2e,%.2e]\n",k,j,p,pmin,pmax);
    //     elseif ( state=="diff" ) then
    //         na = int(log2(real(x)))
    //         nb = int(log2(imag(x)))
    //         nc = int(log2(real(y)))
    //         nd = int(log2(imag(y)))
    //         mprintf("#%d (%d): (2^%5d+%%i*2^%5d)/(2^%5d+%%i*2^%5d), q1=%s, q2=%s\n", ..
    //             j,k,na,nb,nc,nd,..
    //             myformatcomp(q1),myformatcomp(q2));
    //    end
    // endfunction
    //
    // compdiv_searchfordiff ( compdiv_naive , compdiv_scilab ,[],[],[],[], myverbosebrief );
    //
    // Authors
    // Copyright (C) 2011 - Michael Baudin

    function argin = argindefault ( rhs , vararglist , ivar , default )
        // Returns the value of the input argument #ivar.
        // If this argument was not provided, or was equal to the 
        // empty matrix, returns the default value.
        if ( rhs < ivar ) then
            argin = default
        else
            if ( vararglist(ivar) <> [] ) then
                argin = vararglist(ivar)
            else
                argin = default
            end
        end
    endfunction

    function s=myformatd(d)
        if ( isnan(d) | isinf(d) ) then
            s = msprintf("%10s",string(d))
        else
            s = msprintf("%+.3e",d)
        end
    endfunction
    function s=myformatcomp(d)
        s = msprintf("(%12s,%12s)",myformatd(real(d)),myformatd(imag(d)))
    endfunction
    function tf = compdiv_verbosedefault ( k , j , x , y , q1 , q2 , p , s , state , kmax , __cpdsfd_verbose__)

        digits=assert_computedigits(q1,q2,2)
        tf = %f
        if (state=="verb") then
            if ( modulo(k,100)==0) then
                pmin = p-1.96*s
                pmax = p+1.96*s
                mprintf("k=%d, j=%d, p=%.3e, 95%%=[%.2e,%.2e]\n",k,j,p,pmin,pmax);
            end
        elseif ( state=="diff" & __cpdsfd_verbose__ >= 1 ) then
            na = int(log2(real(x)))
            nb = int(log2(imag(x)))
            nc = int(log2(real(y)))
            nd = int(log2(imag(y)))
            mprintf("#%d (%d): (2^%5d+%%i*2^%5d) op (2^%5d+%%i*2^%5d), q1=%s, q2=%s, d=%.2f\n", ..
            k,j,na,nb,nc,nd,..
            myformatcomp(q1),myformatcomp(q2),digits);
        end
    endfunction

    function [j , p, s, k] = compdiv_sfdregular ( x , y , ..
        kmax , div1 , div2 , rtol , atol , __cpdsfd_verbose__ , verbose_type )
        //
        // Setup the verbose function
        if ( verbose_type == "constant" ) then
            __verb_fun__ = compdiv_verbosedefault
            __verb_args__ = list(kmax,__cpdsfd_verbose__)
        elseif ( verbose_type=="function") then
            __verb_fun__ = __cpdsfd_verbose__
            __verb_args__ = list()
        elseif ( verbose_type=="list") then
            __verb_fun__ = __cpdsfd_verbose__f_
            __verb_args__ = __cpdsfd_verbose__(2:$)
        else
            error(msprintf("%s: Internal error: Unexpected function type: %s","compdiv_sfdregular", verbose_type))
        end
        //
        // Initialization
        j=0
        //
        // Loop
        for k = 1 : kmax
            p = j/k
            v = p*(1-p)/k
            s = sqrt(v)
            pmin = p-1.96*s
            pmax = p+1.96*s
            q1 = div1 ( x(k) , y(k) )
            q2 = div2 ( x(k) , y(k) )
            tf = __verb_fun__ ( k , j , x(k) , y(k) , q1 , q2 , p , ..
            s , "verb" , __verb_args__(1:$))
            digits = compdiv_computedigits(q1,q2,2);
            if ( digits < minbits ) then
                j=j+1
                tf = __verb_fun__ ( k , j , x(k) , y(k) , q1 , q2 , p , ..
                s , "diff" , __verb_args__(1:$))
            end
            if ( tf ) then
                break
            end
            if ( abs(pmax-pmin) < rtol * abs(pmax) + atol ) then
                if ( verbose_type == "constant" ) then
                    if ( __cpdsfd_verbose__>=1 ) then
                        mprintf("Required accuracy reached.\n")
                    end
                end
                break
            end
        end
    endfunction
    function [j , p, s, k] = compdiv_sfdblock ( x , y , ..
        kmax , div1 , div2 , rtol , atol , __cpdsfd_verbose__ , verbose_type , ..
        minbits )
        q1 = div1 ( x , y );
        q2 = div2 ( x , y );
        digits = compdiv_computedigits(q1,q2,2);
        failed = find(digits<minbits);
        j = size(failed,"*");
        p = j/kmax;
        v = p*(1-p)/kmax;
        s = sqrt(v);
        pmin = p-1.96*s;
        pmax = p+1.96*s;
        k = kmax
    endfunction



    [lhs, rhs] = argn()
    apifun_checkrhs ( "compdiv_searchfordiff" , rhs , 2:9 )
    apifun_checklhs ( "compdiv_searchfordiff" , lhs , 0:4 )
    //
    // Get arguments
    div1 = varargin ( 1 )
    div2 = varargin ( 2 )
    nmatrix = argindefault ( rhs , varargin , 3 , [-1074:1023;-1074:1023;-1074:1023;-1074:1023])
    kmax = argindefault ( rhs , varargin , 4 , 1000)
    rtol = argindefault ( rhs , varargin , 5 , 1.e-2)  
    atol = argindefault ( rhs , varargin , 6 , 0)  
    __cpdsfd_verbose__ = argindefault ( rhs , varargin , 7 , 0)
    verbose_type = typeof(__cpdsfd_verbose__)
    block = argindefault ( rhs , varargin , 8 , %f )
    minbits = argindefault ( rhs , varargin , 9 , 51 )
    //
    // Check type
    apifun_checktype ( "compdiv_searchfordiff" , div1 , "div1" , 1 , ["function" "fptr"] )
    apifun_checktype ( "compdiv_searchfordiff" , div2 , "div2" , 2 , ["function" "fptr"] )
    apifun_checktype ( "compdiv_searchfordiff" , nmatrix , "nmatrix" , 3 , "constant" )
    apifun_checktype ( "compdiv_searchfordiff" , kmax , "kmax" , 4 , "constant" )
    apifun_checktype ( "compdiv_searchfordiff" , rtol , "rtol" , 5 , "constant" )
    apifun_checktype ( "compdiv_searchfordiff" , atol , "atol" , 6 , "constant" )
    apifun_checktype ( "compdiv_searchfordiff" , __cpdsfd_verbose__ , "__cpdsfd_verbose__" , 7 , ["constant" "function" "list"] )
    if ( verbose_type == "list" ) then
        __cpdsfd_verbose__f_ = __cpdsfd_verbose__(1)
        apifun_checktype ( "compdiv_searchfordiff" , __cpdsfd_verbose__f_, "verbose(1)" , 7 , "function" )
    end
    apifun_checktype ( "compdiv_searchfordiff" , block , "block" , 8 , "boolean" )
    apifun_checktype ( "compdiv_searchfordiff" , minbits , "minbits" , 9 , "constant" )
    //
    // Check size
    m =size(nmatrix,"c")
    apifun_checkdims ( "compdiv_searchfordiff" , nmatrix , "nmatrix" , 3 , [4,m] )
    apifun_checkscalar ( "compdiv_searchfordiff" , kmax , "kmax" , 4 )
    apifun_checkscalar ( "compdiv_searchfordiff" , rtol , "rtol" , 5 )
    apifun_checkscalar ( "compdiv_searchfordiff" , atol , "atol" , 6 )
    if ( verbose_type=="constant") then
        apifun_checkscalar ( "compdiv_searchfordiff" , __cpdsfd_verbose__ , "__cpdsfd_verbose__" , 7 )
    end
    apifun_checkscalar ( "compdiv_searchfordiff" , block , "block" , 8 )
    apifun_checkscalar ( "compdiv_searchfordiff" , minbits , "minbits" , 9 )
    //
    // Check content
    tiny = number_properties("tiny")
    apifun_checkgreq ( "compdiv_searchfordiff" , kmax , "kmax" , 4 , 1 )
    apifun_checkgreq ( "compdiv_searchfordiff" , rtol , "rtol" , 5 , tiny )
    apifun_checkgreq ( "compdiv_searchfordiff" , atol , "atol" , 6 , 0 )
    if ( verbose_type=="constant") then
        apifun_checkoption ( "compdiv_searchfordiff" , __cpdsfd_verbose__ , "__cpdsfd_verbose__" , 7 , [0 1 2 3] )
    end
    apifun_checkgreq ( "compdiv_searchfordiff" , minbits , "minbits" , 9 , 1 )

    //
    m = size(nmatrix,"c")
    na = nmatrix(1,grand(1,kmax,"uin",1,m));
    nb = nmatrix(2,grand(1,kmax,"uin",1,m));
    nc = nmatrix(3,grand(1,kmax,"uin",1,m));
    nd = nmatrix(4,grand(1,kmax,"uin",1,m));
    a=2^na;
    b=2^nb;
    c=2^nc;
    d=2^nd;
    clear na
    clear nb
    clear nc
    clear nd
    //
    // Compute random signs
    sa=grand(1,kmax,"uin",0,1)*2-1;
    sb=grand(1,kmax,"uin",0,1)*2-1;
    sc=grand(1,kmax,"uin",0,1)*2-1;
    sd=grand(1,kmax,"uin",0,1)*2-1;
    a=a.*sa;
    b=b.*sb;
    c=c.*sc;
    d=d.*sd;
    //
    // Create (x,y)
    x=a+%i*b;
    y=c+%i*d;
    clear a
    clear b
    clear c
    clear d
    if ( ~block ) then
        [j , p, s, k] = compdiv_sfdregular ( x , y , ..
        kmax , div1 , div2 , rtol , atol , __cpdsfd_verbose__ , verbose_type )
    else
        [j , p, s, k] = compdiv_sfdblock ( x , y , ..
        kmax , div1 , div2 , rtol , atol , __cpdsfd_verbose__ , verbose_type , minbits )
    end
    if ( verbose_type == "constant" ) then
        if ( __cpdsfd_verbose__>=1 ) then
            mprintf("End of the algorithm.\n")
            pmin = p-1.96*s
            pmax = p+1.96*s
            mprintf("k=%d, j=%d, p=%.3e, 95%%=[%.2e,%.2e]\n",k,j,p,pmin,pmax);
        end
    end
endfunction

