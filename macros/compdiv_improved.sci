// Copyright (C) 2010-2011 - Robert L. Smith - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function z = compdiv_improved ( x, y )
    // Improved Complex Division - Robert L. Smith - Michael Baudin - 2010-2011
    //
    // Calling Sequence
    // z = compdiv_improved ( x,y )
    //
    // Parameters
    // x : a matrix of complex doubles
    // y : a matrix of complex doubles
    // z : a matrix of complex doubles, the complex division result z=x/y.
    // 
    // Description
    // An improved complex division, based on a improved 
    // Smith algorithm, with an additionnal case.
    // This is the algorithm presented in the paper "Improved Complex Division". 
    // We consider additionnal cases which take into account for the 
    // condition r<>0, where z = d/c or z=c/d.
    //
    // Examples
    // expected = 11/25 + %i * 2/25
    // compdiv_improved ( 1+2*%i , 3+4*%i )
    //
    // x = rand(3,5)+%i*rand(3,5)
    // y = rand(3,5)+%i*rand(3,5)
    // r = compdiv_improved ( x , y )
    // abs(r.*y - x)./abs(x)
    //
    // Authors
    // Copyright (C) 2010-2011 - Robert L. Smith - Michael Baudin

    for i = 1 : size(x,"r")
    for j = 1 : size(x,"c")
    z(i,j) = compdiv_improvedS ( x(i,j) , y(i,j) )
    end
    end
endfunction
function z = compdiv_improvedS ( x, y )
// Set z := x/y
    a = real(x)
    b = imag(x)
    c = real(y)
    d = imag(y)
    if ( abs(d) <= abs(c) ) then
        [e,f] = improved_internal(a,b,c,d)
    else
        [e,f] = improved_internal(b,a,d,c)
        f = -f
    end
    // Avoid using x + %i * y, which may create
    // %inf+%i*%inf, which actually multiplies 0 and %inf,
    // generating an unwanted %nan.
    z = complex(e,f)
endfunction
function [e,f] = improved_internal(a,b,c,d)
// Set e + i f := (a + i b) / (c + i d)
// when |d|<= |c|.
    r = d/c
    t = 1/(c + d * r)
    if (r <> 0) then
        e = (a + b * r) * t
        f = (b - a * r) * t
    else
        e = (a + d * (b/c)) * t
        f = (b - d * (a/c)) * t
    end
endfunction


