// Copyright (C) 2010-2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

#ifndef _COMPDIV_H_
#define _COMPDIV_H_

#ifdef _MSC_VER
	#if LIBCOMPDIV_EXPORTS 
		#define COMPDIV_IMPORTEXPORT __declspec (dllexport)
	#else
		#define COMPDIV_IMPORTEXPORT __declspec (dllimport)
	#endif
#else
	#define COMPDIV_IMPORTEXPORT
#endif

#undef __BEGIN_DECLS
#undef __END_DECLS
#ifdef __cplusplus
# define __BEGIN_DECLS extern "C" {
# define __END_DECLS }
#else
# define __BEGIN_DECLS /* empty */
# define __END_DECLS /* empty */
#endif

__BEGIN_DECLS

// Sets z = x/y, where x, y and z are complex numbers:
// x=x[0] + I * x[1]
// y=y[0] + I * y[1]
// z=z[0] + I * z[1]
void compdiv_priest(const double *x, const double *y, double *z);
void compdiv_improved(const double *x, const double *y, double *z);
void compdiv_ansiisoc(const double * x, const double * y, double * z );
void compdiv_robust(const double *x, const double *y, double *z);
void compdiv_stewart(const double *x, const double *y, double *z);

__END_DECLS


#endif /* _COMPDIV_H_ */

