// Copyright (C) 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


// References
//
// "Efficient scaling for complex division"
// Douglas M. Priest
// Sun Microsystems
// ACM Transactions on Mathematical software, Vol. 30, No. 4, December 2004


// A driver for Priest's division
/* 
  To compile :

  $ gcc -c cdiv_priest.c
  $ gcc -c cdiv_priest-driver.c 
  $ gcc -o priest.exe cdiv_priest.o cdiv_priest-driver.o
  $ ./priest.exe
(1.000000e+00+I*2.000000e+00)/(3.000000e+00+I*4.000000e+00)=4.400000e-01+I*8.000000e-02

*/

#include <stdio.h>
#include <stdlib.h>

// Set v[0] + i v[1] := (z[0] + i z[1]) / (w[0] + i w[1]) assuming:
void cdiv(double *v, const double *z, const double *w);

int main(void)
{
    double v[2];
    double z[2];
    double w[2];
    
    // Perform (1+i*2)/(3+i*4)
    z[0]=1;
    z[1]=2;
    w[0]=3;
    w[1]=4;
    cdiv(v, z, w);
    printf("(%e+I*%e)/(%e+I*%e)=%e+I*%e\n", z[0],z[1],w[0],w[1],v[0],v[1]);
    
    return EXIT_SUCCESS;
}

