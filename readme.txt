Compdiv toolbox

Purpose
-------

The goal of this toolbox is to a collection 
of algorithms for complex division.

This project is linked to the paper "Scilab is not naive", Baudin, 2010.

This is an EXPERIMENTAL project.
Many of the algorithms provided in this module do not work...

See the overview in the help provided with this toolbox.

Features
--------

 * compdiv_overview — An overview of the Compdiv toolbox.
 * compdiv_ansiisoc — Performs complex division with a port of ANSI/ISO C algorithm.
 * compdiv_fansiisoc — Performs fast complex division with ANSI/ISO C99 algorithm.
 * compdiv_fimproved — Performs fast complex division with improved algorithm.
 * compdiv_frobust — Performs fast complex division with robust algorithm.
 * compdiv_fstewart — Performs fast complex division with Stewart's algorithm.
 * compdiv_improved — Improved Complex Division - Robert L. Smith - Michael Baudin - 2010-2011
 * compdiv_naive — Performs complex division with a naive algorithm.
 * compdiv_polar — A complex division based on polar form.
 * compdiv_priest — Performs fast complex division with Priest's algorithm.
 * compdiv_robust — Improved Complex Division - Robert L. Smith - Michael Baudin - 2010-2011
 * compdiv_scilab — Performs the complex division with Scilab slash.
 * compdiv_smith — Returns the complex division z = x / y by Smith's method.
 * compdiv_smithLi — Performs complex division with a Smith method, improved by Li et al.
 * compdiv_smithR — Performs complex division with a Smith method, as implemented in R.
 * compdiv_smithSZ — Simulates Smith's algorithm on a store-zero system.
 * compdiv_stewart — Performs complex division with Stewart's algorithm.
 * Product
   * compdiv_mulansic — Performs complex multiplication with a ANSI/ISO C99 algorithm.
   * compdiv_mulnaive — Performs complex multiplication with a naive algorithm.
   * compdiv_mulscilab — Performs the complex multiplication with Scilab star.
 * Support
   * compdiv_copysign — Returns r with the magnitude of x and the sign of y.
   * compdiv_difficultcases — A small collection of difficult complex divisions.
   * compdiv_evaluate — Evaluate intermediate expressions different formulas.
   * compdiv_extreme — Test extreme complex divisions.
   * compdiv_hypot — Square root of sum of squares.
   * compdiv_isfinite — Array elements that are finite.
   * compdiv_maclaren — A benchmark suggested by Mac Laren.
   * compdiv_pow2 — Base 2 power and scale floating-point numbers.
   * compdiv_prodexplog — Returns the product of the entries in x.
   * compdiv_prodfrexp — Returns the product of the entries in x.
   * compdiv_prodminmax — Computes the product, avoiding overflows or underflows.
   * compdiv_searchfordiff — Search for differences between 2 algorithms.
   * compdiv_storezero — Returns the floating point number fl(x) on a store-zero system.
   * compdiv_testdataset — Test a complex division on a dataset
   * compdiv_tryevaluate — Try to evaluate all the methods for expression.

Dependencies
------------

 * This module depends on the apifun module.
 * This module depends on the helptbx module.

Author
------

2009-2011 - Michael Baudin

Licence
-------

This toolbox is distributed under the CeCILL license.

Acknowledgements
----------------

I thank Robert L. Smith for his suggestions and support. 
I thank James Demmel for his numerical experiments. 
I thank Nicholas Higham for his explanations on complex 
error bounds. 

Bibliography
------------

Problems with Nans and Infs in the result of Octave.
 * Signed zeros and Infi: http://octave.1599824.n4.nabble.com/Signed-zeros-and-Infi-td1662707.html
 * FP exception when dividing by small complex number: http://octave.1599824.n4.nabble.com/FP-exception-when-dividing-by-small-complex-number-td1661147.html
 * Inconsistency in complex division by zero: http://octave.1599824.n4.nabble.com/Inconsistency-in-complex-division-by-zero-td1665328.html

