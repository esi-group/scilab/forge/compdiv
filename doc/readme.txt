Documentation : Improved Complex Division

Abstract
--------

This document presents an improved, but simple, complex 
division. 
We present a proof of the robustness of the improved 
algorithm. 
Numerical simulations show that the algorithm performs 
well in practice and is significantly more accurate 
than other known implementations.

Authors
-------

Copyright (C) 2009-2011 - DIGITEO - Michael Baudin
Copyright (C) 2009-2011 - Stanford University - Robert Smith

Licence
-------

This document is released under the terms of the Creative Commons Attribution-ShareAlike 
3.0 Unported License :

http://creativecommons.org/licenses/by-sa/3.0/

