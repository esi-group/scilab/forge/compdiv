// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Computing worst case complex multiplication

function z = compmul ( x , y )
  a = real(x)
  b = imag(x)
  c = real(y)
  d = imag(y)
  z = complex(a*c-b*d,a*d+b*c)
endfunction
x = 1 + 2*%i
y = 3 + 4*%i
e = -5+10*%i
c = compmul ( x , y )
abs(c-e)/abs(e)/u

// {E}rror {B}ounds on {C}omplex {F}loating-{P}oint {M}ultiplication}
// {B}rent, {R}ichard and {P}ercival, {C}olin and {Z}immermann, {P}aul},

// Worst case: Corollary 5
// ((3/4)*(1+4*2^-53)+i*(3/4)) * ((2/3)*(1+7*2^-53)+i*(2/3)*(1+2^-53))
a0=(3/4)*(1+4*u)
b0=3/4
a1=(2/3)*(1+7*u)
b1=(2/3)*(1+u)
x = complex(a0,b0)
y = complex(a1,b1)
c = x*y
e = 5.551115123125784427e-16 + 1.000000000000000666 * %i
abs(c-e)/abs(e)/u
//
c2 = compmul ( x , y )
abs(c2-e)/abs(e)/u
//
// Try with eps
// ((3/4)*(1+4*2^-52)+i*(3/4)) * ((2/3)*(1+7*2^-52)+i*(2/3)*(1+2^-52))
a0=(3/4)*(1+4*%eps)
b0=3/4
a1=(2/3)*(1+7*%eps)
b1=(2/3)*(1+%eps)
x = complex(a0,b0)
y = complex(a1,b1)
c = x*y
e = 1.110223024625157230e-15 + 1.000000000000001332 * %i
abs(c-e)/abs(e)/u
//
c2 = compmul ( x , y )
abs(c2-e)/abs(e)/u
