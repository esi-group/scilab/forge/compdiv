Documentation : Improved Complex Division

Copyright (C) 2009-2011 - DIGITEO - Michael Baudin
Copyright (C) 2009-2011 - Stanford University - Robert Smith

This document is released under the terms of the Creative Commons Attribution-ShareAlike 
3.0 Unported License :

http://creativecommons.org/licenses/by-sa/3.0/

